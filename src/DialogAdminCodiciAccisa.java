/*
 * DialogAdminCodiciAccisa.java
 *
 * Created on 1 aprile 2009, 12.53
 */

/**
 *
 * @author  ginannaschi
 */
import java.sql.*;
import java.util.*;
import java.io.*;
import java.awt.*;
import java.lang.*;
import javax.swing.*;
import javax.swing.table.*;

public class DialogAdminCodiciAccisa extends javax.swing.JDialog {
    
    private RecordJavaDb rcorr = null;
    private Vector v = null;
    
    /** Creates new form DialogAdminCodiciAccisa */
    public DialogAdminCodiciAccisa(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        documentoTesto1 = new halleytech.models.DocumentoTesto();
        documentoTesto2 = new halleytech.models.DocumentoTesto();
        documentoTesto3 = new halleytech.models.DocumentoTesto();
        documentoTesto4 = new halleytech.models.DocumentoTesto();
        documentoTesto5 = new halleytech.models.DocumentoTesto();
        documentoTesto6 = new halleytech.models.DocumentoTesto();
        documentoTesto7 = new halleytech.models.DocumentoTesto();
        documentoTesto8 = new halleytech.models.DocumentoTesto();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        codiceaccisa = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        codiceutente = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        ragionesociale = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        tipoinvio = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        tipologia = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        buttonIns = new halleylib.beans.NftButton();
        buttonCanc = new halleylib.beans.NftButton();
        licenza = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        percorsofile = new javax.swing.JTextField();
        buttonRicFile = new halleylib.beans.NftButton();

        documentoTesto1.scriviLung(13);

        documentoTesto2.scriviLung(4);

        documentoTesto4.scriviLung(20);

        documentoTesto5.scriviLung(8);

        documentoTesto6.scriviLung(8);

        documentoTesto7.scriviLung(8);

        documentoTesto8.scriviLung(8);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Gestione Codici Accisa");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 11)); // NOI18N
        jLabel1.setText("Elenco codici accisa registrati:");
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        getContentPane().add(jLabel1);
        jLabel1.setBounds(10, 0, 230, 20);

        tab.setBackground(new java.awt.Color(255, 255, 232));
        tab.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        tab.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tab);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(10, 20, 630, 260);

        codiceaccisa.setDocument(documentoTesto1);
        codiceaccisa.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        getContentPane().add(codiceaccisa);
        codiceaccisa.setBounds(130, 290, 140, 20);

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Codice accisa");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(8, 288, 120, 20);

        jLabel4.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Codice utente autorizzato");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(330, 290, 130, 20);

        codiceutente.setDocument(documentoTesto2);
        codiceutente.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        getContentPane().add(codiceutente);
        codiceutente.setBounds(460, 290, 70, 20);

        jLabel5.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Cartella files");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(0, 370, 130, 20);

        ragionesociale.setDocument(documentoTesto3);
        ragionesociale.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        getContentPane().add(ragionesociale);
        ragionesociale.setBounds(130, 310, 400, 20);

        jLabel6.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Tipologia");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(0, 330, 130, 20);

        tipoinvio.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        tipoinvio.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Giornaliero", "Mensile" }));
        getContentPane().add(tipoinvio);
        tipoinvio.setBounds(420, 330, 110, 20);

        jLabel7.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Modalità invio");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(320, 330, 100, 20);

        tipologia.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        tipologia.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "OLIMDA", "OLIMDC", "OLIMOP", "OLLUDA", "ALCODA", "ALCODC", "ALCOAV", "ALCOPP", "ALCOAR", "ALCOMB" }));
        tipologia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tipologiaActionPerformed(evt);
            }
        });
        getContentPane().add(tipologia);
        tipologia.setBounds(130, 330, 140, 20);

        jLabel8.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Codice licenza");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(10, 350, 120, 20);

        buttonIns.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/png/16x16/play.png"))); // NOI18N
        buttonIns.setText("INS");
        buttonIns.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        buttonIns.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonInsActionPerformed(evt);
            }
        });
        getContentPane().add(buttonIns);
        buttonIns.setBounds(560, 290, 80, 50);

        buttonCanc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/png/16x16/trash.png"))); // NOI18N
        buttonCanc.setText("CANC");
        buttonCanc.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        buttonCanc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCancActionPerformed(evt);
            }
        });
        getContentPane().add(buttonCanc);
        buttonCanc.setBounds(560, 340, 80, 50);

        licenza.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        getContentPane().add(licenza);
        licenza.setBounds(130, 350, 140, 20);

        jLabel9.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Ragione sociale");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(0, 310, 130, 20);

        percorsofile.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        getContentPane().add(percorsofile);
        percorsofile.setBounds(130, 370, 380, 20);

        buttonRicFile.setBackground(new java.awt.Color(182, 181, 193));
        buttonRicFile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/zoom.png"))); // NOI18N
        buttonRicFile.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        buttonRicFile.setOpaque(false);
        buttonRicFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRicFileActionPerformed(evt);
            }
        });
        getContentPane().add(buttonRicFile);
        buttonRicFile.setBounds(510, 370, 20, 20);

        setSize(new java.awt.Dimension(665, 443));
    }// </editor-fold>//GEN-END:initComponents

    private void buttonCancActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCancActionPerformed
        if (tab.getRowCount() > 0 && tab.getSelectedRow() >= 0)
        {
            int ris = JOptionPane.showConfirmDialog(this, "Cancellare posizione selezionata?",
                "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (ris == JOptionPane.YES_OPTION)
            {
                RecordJavaDb r = (RecordJavaDb) v.elementAt(tab.getSelectedRow());
                try
                {
                    Statement st = EnvJAcciseNet.conn.createStatement();
                    st.execute("DELETE FROM codaccisa WHERE id = " + r.leggiIntero("id"));
                    st.close();
                    this.mostraDati();
                    this.preparaNuovo();
                }
                catch (Exception e)
                {
                    JOptionPane.showMessageDialog(this, "Errore su aggiornamento:\n" + e.getMessage(), "Errore",
                        JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }//GEN-LAST:event_buttonCancActionPerformed

    private void tabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMouseClicked
        if (tab.getRowCount() > 0 && tab.getSelectedRow() >= 0 && evt.getClickCount() == 2)
        {
            RecordJavaDb r = (RecordJavaDb) v.elementAt(tab.getSelectedRow());
            codiceaccisa.setText(r.leggiStringa("codiceaccisa"));
            codiceutente.setText(r.leggiStringa("codiceutente"));
            ragionesociale.setText(r.leggiStringa("ragionesociale"));
            tipologia.setSelectedItem(r.leggiStringa("tipologia"));
            if (r.leggiIntero("tipoinvio") == 1)
                tipoinvio.setSelectedIndex(0);
            else
                tipoinvio.setSelectedIndex(1);
            licenza.setSelectedItem(r.leggiStringa("licenza"));
            percorsofile.setText(r.leggiStringa("percorsofile"));
            rcorr = r;
            buttonIns.setText("VAR");
            codiceaccisa.requestFocus();
        }
    }//GEN-LAST:event_tabMouseClicked

    private void buttonInsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonInsActionPerformed
        if (codiceaccisa.getText().equals(""))
        {
            JOptionPane.showMessageDialog(this, "Specificare codice accisa", "Errore",
                JOptionPane.ERROR_MESSAGE);
            codiceaccisa.requestFocus();
            return;
        }
        if (codiceaccisa.getText().length() != 13)
        {
            JOptionPane.showMessageDialog(this, "Codice accisa di lunghezza errata", "Errore",
                JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (codiceutente.getText().equals(""))
        {
            JOptionPane.showMessageDialog(this, "Specificare codice utente", "Errore",
                JOptionPane.ERROR_MESSAGE);
            codiceutente.requestFocus();
            return;
        }
        if (codiceutente.getText().length() != 4)
        {
            JOptionPane.showMessageDialog(this, "Codice utente di lunghezza errata", "Errore",
                JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (ragionesociale.getText().equals(""))
        {
            JOptionPane.showMessageDialog(this, "Specificare ragione sociale", "Errore",
                JOptionPane.ERROR_MESSAGE);
            ragionesociale.requestFocus();
            return;
        }
        if (rcorr != null)
        {
            String nl = (String) licenza.getSelectedItem();
            System.out.println("nl:" + nl);
            try
            {
                int totl = 0;
                Statement st = EnvJAcciseNet.conn.createStatement();
                ResultSet rs = st.executeQuery("SELECT nposizioni FROM licenze WHERE licenza = '" + nl + "'");
                rs.next();
                totl = rs.getInt("nposizioni");
                st.close();
                System.out.println("totl:" + totl);
                
                int nlatt = 0;
                st = EnvJAcciseNet.conn.createStatement();
                rs = st.executeQuery("SELECT COUNT(*) AS num FROM codaccisa WHERE licenza = '" + nl + "' AND id <> "  + rcorr.leggiIntero("id"));
                if (rs.next())
                {
                    nlatt = rs.getInt("num");
                }
                st.close();
                nlatt++;
                System.out.println("nlatt:" + nlatt);
                if (nlatt > totl)
                {
                    JOptionPane.showMessageDialog(this, "Gi� raggiunto numero massimo posizioni attivabili (" + totl + ")", "Errore",
                        JOptionPane.ERROR_MESSAGE);
                    ragionesociale.requestFocus();
                    return;
                }
            }
            catch (Exception ee)
            {
            }
            try
            {
                PreparedStatement pst = EnvJAcciseNet.conn.prepareStatement(
                    "UPDATE codaccisa SET codiceaccisa=?,codiceutente=?,ragionesociale=?,tipologia=?,tipoinvio=?,licenza=?,percorsofile=? " +
                    "WHERE id=?");
                pst.clearParameters();
                pst.setString(1, codiceaccisa.getText());
                pst.setString(2, codiceutente.getText());
                pst.setString(3, ragionesociale.getText());
                pst.setString(4, (String) tipologia.getSelectedItem());
                pst.setInt(5, tipoinvio.getSelectedIndex() + 1);
                pst.setString(6, (String) licenza.getSelectedItem());
                pst.setString(7, percorsofile.getText());
                pst.setInt(8, rcorr.leggiIntero("id"));
                pst.execute();
                pst.close();
            }
            catch (Exception e)
            {
                JOptionPane.showMessageDialog(this, "Errore su aggiornamento:\n" + e.getMessage(), "Errore",
                    JOptionPane.ERROR_MESSAGE);
            }
        }
        else
        {
            String nl = (String) licenza.getSelectedItem();
            try
            {
                int totl = 0;
                Statement st = EnvJAcciseNet.conn.createStatement();
                ResultSet rs = st.executeQuery("SELECT nposizioni FROM licenze WHERE licenza = '" + nl + "'");
                rs.next();
                totl = rs.getInt("nposizioni");
                st.close();
                int nlatt = 0;
                st = EnvJAcciseNet.conn.createStatement();
                rs = st.executeQuery("SELECT COUNT(*) AS num FROM codaccisa WHERE licenza = '" + nl + "'");
                if (rs.next())
                {
                    nlatt = rs.getInt("num");
                }
                st.close();
                if (nlatt == totl)
                {
                    JOptionPane.showMessageDialog(this, "Gi� raggiunto numero massimo posizioni attivabili (" + totl + ")", "Errore",
                        JOptionPane.ERROR_MESSAGE);
                    ragionesociale.requestFocus();
                    return;
                }
            }
            catch (Exception ee)
            {
            }
            try
            {
                PreparedStatement pst = EnvJAcciseNet.conn.prepareStatement(
                    "INSERT INTO codaccisa (codiceaccisa,codiceutente,ragionesociale,tipologia,tipoinvio,licenza,percorsofile) " +
                    "VALUES (?,?,?,?,?,?,?)");
                pst.clearParameters();
                pst.setString(1, codiceaccisa.getText());
                pst.setString(2, codiceutente.getText());
                pst.setString(3, ragionesociale.getText());
                pst.setString(4, (String) tipologia.getSelectedItem());
                pst.setInt(5, tipoinvio.getSelectedIndex() + 1);
                pst.setString(6, (String) licenza.getSelectedItem());
                pst.setString(7, percorsofile.getText());
                pst.execute();
                pst.close();
            }
            catch (Exception e)
            {
                JOptionPane.showMessageDialog(this, "Errore su inserimento:\n" + e.getMessage(), "Errore",
                    JOptionPane.ERROR_MESSAGE);
            }
        }
        this.mostraDati();
        this.preparaNuovo();
        codiceaccisa.requestFocus();
    }//GEN-LAST:event_buttonInsActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        try
        {
            licenza.removeAllItems();
            Statement st = EnvJAcciseNet.conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM licenze ORDER BY licenza");
            while (rs.next())
            {
                licenza.addItem(rs.getString("licenza"));
            }
            st.close();
        }
        catch (Exception e)
        {
        }
        if (licenza.getItemCount() == 0)
        {
            JOptionPane.showMessageDialog(this, "Nessuna licenza inserita", "Errore",
                JOptionPane.ERROR_MESSAGE);
            dispose();
            return;
        }
        this.mostraDati();
        this.preparaNuovo();
        codiceaccisa.requestFocus();
    }//GEN-LAST:event_formWindowOpened

    private void buttonRicFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRicFileActionPerformed
        JFileChooser fc = new JFileChooser();
        fc.setDialogType(JFileChooser.CUSTOM_DIALOG);
        fc.setDialogTitle("Scelta percorso generazione file");
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if (fc.showDialog((Frame) this.getParent(), "OK") == JFileChooser.APPROVE_OPTION)
            percorsofile.setText(fc.getSelectedFile().getPath());
}//GEN-LAST:event_buttonRicFileActionPerformed

    private void tipologiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tipologiaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tipologiaActionPerformed
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        new DialogAdminCodiciAccisa(new javax.swing.JFrame(), true).show();
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private halleylib.beans.NftButton buttonCanc;
    private halleylib.beans.NftButton buttonIns;
    private halleylib.beans.NftButton buttonRicFile;
    private javax.swing.JTextField codiceaccisa;
    private javax.swing.JTextField codiceutente;
    private halleytech.models.DocumentoTesto documentoTesto1;
    private halleytech.models.DocumentoTesto documentoTesto2;
    private halleytech.models.DocumentoTesto documentoTesto3;
    private halleytech.models.DocumentoTesto documentoTesto4;
    private halleytech.models.DocumentoTesto documentoTesto5;
    private halleytech.models.DocumentoTesto documentoTesto6;
    private halleytech.models.DocumentoTesto documentoTesto7;
    private halleytech.models.DocumentoTesto documentoTesto8;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox licenza;
    private javax.swing.JTextField percorsofile;
    private javax.swing.JTextField ragionesociale;
    private javax.swing.JTable tab;
    private javax.swing.JComboBox tipoinvio;
    private javax.swing.JComboBox tipologia;
    // End of variables declaration//GEN-END:variables
    
    void preparaNuovo()
    {
        rcorr = null;
        buttonIns.setText("INS");
        codiceaccisa.setText("");
        codiceutente.setText("");
        ragionesociale.setText("");
        percorsofile.setText("");
        tipologia.setSelectedIndex(0);
        tipoinvio.setSelectedIndex(0);
        licenza.setSelectedIndex(0);
    }
    
    void mostraDati()
    {
        try
        {
            v = new Vector();
            Statement st = EnvJAcciseNet.conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM codaccisa ORDER BY codiceaccisa");
            while (rs.next())
            {
                RecordJavaDb r = new RecordJavaDb(rs);
                v.addElement(r);
            }
            st.close();
            final String[] names = {"Codice accisa", "Cod.utente", "Rag.sociale", "Tipologia", "Invio", "Codice licenza", "Percorso file"};
            final Object[][] data = new Object[v.size()][7];
            for (int i = 0; i < v.size(); i++)
            {
                RecordJavaDb r = (RecordJavaDb) v.elementAt(i);
                data[i][0] = r.leggiStringa("codiceaccisa");
                data[i][1] = r.leggiStringa("codiceutente");
                data[i][2] = r.leggiStringa("ragionesociale");
                data[i][3] = r.leggiStringa("tipologia");
                if (r.leggiIntero("tipoinvio") == 1)
                    data[i][4] = "Giornaliero";
                else
                    data[i][4] = "Mensile";
                data[i][5] = r.leggiStringa("licenza");
                data[i][6] = r.leggiStringa("percorsofile");
            }
            TableModel modello = new AbstractTableModel() {
                public int getColumnCount() { return names.length; } 
                public int getRowCount() { return data.length;}
                public Object getValueAt(int row, int col) {return data[row][col];}
                public String getColumnName(int column) {return names[column];}
                public Class getColumnClass(int col) {return getValueAt(0,col).getClass();}
                public boolean isCellEditable(int row, int col) {return false;} 
                public void setValueAt(Object aValue, int row, int column) {
                    data[row][column] = aValue; 
                }
            }; 
            tab.setModel(modello); 
            tab.getColumnModel().getColumn(0).setPreferredWidth(120);
            tab.getColumnModel().getColumn(1).setPreferredWidth(60);
            tab.getColumnModel().getColumn(2).setPreferredWidth(200);
            tab.getColumnModel().getColumn(3).setPreferredWidth(80);
            tab.getColumnModel().getColumn(4).setPreferredWidth(70);
            tab.getColumnModel().getColumn(5).setPreferredWidth(100);
            tab.getColumnModel().getColumn(6).setPreferredWidth(200);
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(this, "Errore vis.dati accise caricate:\n" + e.getMessage(), "Errore",
                JOptionPane.ERROR_MESSAGE);
        }
    }
}
