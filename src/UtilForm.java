import java.awt.*;
import javax.swing.*;
import java.util.*;

public class UtilForm
{
    public static final int ALTO_SINISTRA = 0;
    public static final int ALTO_DESTRA = 1;
    public static final int BASSO_SINISTRA = 2;
    public static final int BASSO_DESTRA = 3;
    public static final int CENTRO = 4;
    public static final int SX = 5;
    public static final int DX = 6;
    
    public static void posRelSchermo(Window w, int modo)
    {
        try
        {
            Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
            int dimSchermoX = d.width;
            int dimSchermoY = d.height;

            switch (modo)
            {
                case CENTRO:
                {
                    w.setLocation((dimSchermoX - w.getSize().width) / 2, (dimSchermoY - w.getSize().height) / 2);
                    break;
                }
                case ALTO_SINISTRA:
                {
                    w.setLocation(0, 0);
                    break;
                }
                case ALTO_DESTRA:
                {
                    w.setLocation(dimSchermoX - w.getSize().width, 0);
                    break;
                }
                case BASSO_SINISTRA:
                {
                    w.setLocation(0, dimSchermoY - w.getSize().height);
                    break;
                }
                case BASSO_DESTRA:
                {
                    w.setLocation(dimSchermoX - w.getSize().width, dimSchermoY - w.getSize().height);
                    break;
                }
            }
        }
        catch (Exception e)
        {
        }
    }

    public static void posRelPadre(Window w, Window padre, int modo)
    {
        try
        {
            if (padre != null)
            {
                int posXPadre = padre.getLocation().x;
                int posYPadre = padre.getLocation().y;
                int dimXPadre = padre.getSize().width;
                int dimYPadre = padre.getSize().height;
                switch (modo)
                {
                    case CENTRO:
                    {
                        w.setLocation(posXPadre + (dimXPadre - w.getSize().width) / 2,
                            posYPadre + (dimYPadre - w.getSize().height) / 2);
                        break;
                    }
                    case ALTO_SINISTRA:
                    {
                        w.setLocation(posXPadre, posYPadre);
                        break;
                    }
                    case ALTO_DESTRA:
                    {
                        w.setLocation(posXPadre + dimXPadre - w.getSize().width, posYPadre);
                        break;
                    }
                    case BASSO_SINISTRA:
                    {
                        w.setLocation(posXPadre, posYPadre + dimYPadre - w.getSize().height);
                        break;
                    }
                    case BASSO_DESTRA:
                    {
                        w.setLocation(posXPadre + dimXPadre - w.getSize().width, posYPadre + dimYPadre - w.getSize().height);
                        break;
                    }
                }
            }
        }
        catch (Exception e)
        {
        }
    }

    public static void posRelComponente(Window w, Component c)
    {
        try
        {
            w.setLocation(c.getLocationOnScreen());
        }
        catch (Exception e)
        {
        }
    }

    public static void posRelComponente(Window w, Component c, int align)
    {
        if (align == SX)
        {
            try
            {
                w.setLocation(c.getLocationOnScreen());
            }
            catch (Exception e)
            {
            }
        }
        else
        {
            try
            {
                Point loc = c.getLocationOnScreen();
                loc.setLocation(loc.getX() - w.getSize().width, loc.getY());
                w.setLocation(loc);
            }
            catch (Exception e)
            {
            }
        }
    }


    public static void adattaDialogRicerca(Container contenitore, JScrollPane pScroll, JTable tab)
    {
        pScroll.setSize(contenitore.getWidth() - 8, contenitore.getHeight() - pScroll.getY() - 4);
        //pScroll.setPreferredSize(new Dimension(contenitore.getWidth() - 8, contenitore.getHeight() - pScroll.getY() - 4));
        //pScroll.getViewport().setPreferredSize(new Dimension(contenitore.getWidth() - 8, contenitore.getHeight() - pScroll.getY() - 4));
        if (tab != null)
        {
            //tab.setSize(contenitore.getWidth() - 8, contenitore.getHeight() - pScroll.getY() - 4);
            //tab.setPreferredSize(new Dimension(contenitore.getWidth() - 8, contenitore.getHeight() - pScroll.getY() - 4));
            tab.revalidate();
        }
    }
}
