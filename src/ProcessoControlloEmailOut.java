/*
 * ProcessoControlloFTP.java
 *
 * Created on 6 aprile 2009, 14.33
 */

import java.sql.*;
import java.util.*;
import java.awt.*;
import java.lang.*;
import java.io.*;
import java.text.*;

/**
 *
 * @author  ginannaschi
 */
public class ProcessoControlloEmailOut extends Thread
{
    public String pcc = "";
    public long intervallo = 5000;
    public String server_smtp = "";
    public int porta_smtp = 25;
    public String username = "";
    public String password = "";
    public String mittente = "";
    public String destinatario = "";
    public boolean uscita = false;
    private Connection conn2 = null;
    
    public ProcessoControlloEmailOut(String pcc, long intervallo, String server_smtp, int porta_smtp, String username, String password,
        String mittente, String destinatario)
    {
        this.pcc = pcc;
        this.server_smtp = server_smtp;
        this.porta_smtp = porta_smtp;
        this.username = username;
        this.password = password;
        this.mittente = mittente;
        this.destinatario = destinatario;
        this.intervallo = intervallo;
    }
    
    public void run()
    {
        try
        {
            conn2 = DriverManager.getConnection("jdbc:derby://localhost:" + EnvJAcciseNet.portadb + "/jazzaccise_server");
        }
        catch (Exception e)
        {
        }
        while (!uscita)
        {
            //System.out.println("uscita: " + uscita);
            try
            {
                //System.out.println("Controllo invio email in corso...");
                File f = new File(pcc);
                Statement st = conn2.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM codaccisa");
                while (rs.next())
                {
                    String codacc = rs.getString("codiceaccisa");
                    System.out.println("controllo " + codacc + rs.getString("tipologia").trim());
                    // cerca files su cartella con codice accisa corrente
                    File facc = new File(pcc + File.separator + codacc + rs.getString("tipologia").trim());
                    if (facc.exists() && facc.isDirectory())
                    {
                        String[] files = facc.list();
                        System.out.println( "N.files in dir " + codacc + rs.getString("tipologia").trim() + ": " + files.length);
                        // copia files
                        for (int i = 0; i < files.length; i++)
                        {
                            File ff = new File(pcc + File.separator + codacc + rs.getString("tipologia").trim() + File.separator + files[i]);
                            //svuota cartella temporanea
                            File ftmp = new File("tmp" + codacc + rs.getString("tipologia").trim());
                            if (!ftmp.exists())
                                ftmp.mkdir();
                            ftmp = new File("tmp" + codacc + rs.getString("tipologia").trim());
                            String[] lftmp = ftmp.list();
                            for (int j = 0; j < lftmp.length; j++)
                            {
                                File fd = new File("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + lftmp[j]);
                                fd.delete();
                            }
                            this.sleep(3000);
                            // tenta invio file per email
                            String[] all = new String[1];
                            all[0] = pcc + File.separator + codacc + rs.getString("tipologia").trim() + File.separator + files[i];
                            String[] ris = FunzioniEmail.inviaEmail(server_smtp, porta_smtp, username, password, mittente, destinatario,
                                "JACCISE " + files[i], "INVIO FILE " + files[i], all);
                            if (ris[0].equals("S"))
                            {
                                // cancella file
                                ff.delete();
                                // registra su db in tabella files_in
                                PreparedStatement pst = conn2.prepareStatement(
                                    "INSERT INTO email_out (nomefile,codutente,codiceaccisa,tipologia,datainvio,orainvio) VALUES (?,?,?,?,?,?)");
                                pst.clearParameters();
                                pst.setString(1, files[i]);
                                pst.setString(2, files[i].substring(0, 4));
                                pst.setString(3, codacc);
                                pst.setString(4, rs.getString("tipologia"));
                                pst.setString(5, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                                SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                                pst.setString(6, df.format(new java.util.Date()));
                                pst.execute();
                                pst.close();
                            }
                        }
                    }    
                    else
                    {
                        // crea la directory
                        boolean mkd = facc.mkdir();
                        if (mkd)
                        {
                            System.out.println("invio email: creata directory " + codacc + rs.getString("tipologia").trim());
                        }
                    }
                }
                st.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            try
            {
                this.sleep(intervallo);
            }
            catch (Exception esleep)
            {
            }
        }
        try
        {
            conn2.close();
        }
        catch (Exception ec)
        {
        }
    }
}
