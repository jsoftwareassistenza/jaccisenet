/*
 * SAccisa.java
 *
 * Created on 13 marzo 2008, 15.47
 */


import java.security.*;
import java.lang.*;
/**
 *
 * @author  ginanneschi od. by campinotti
 */
public class SAccisa {
    
    /** Creates a new instance of SAccisa */
    public SAccisa() {
    }
    
    public static final String calcoloChiaveAttivazione(String pivacf, int tipoinst, boolean singolaazienda, String nlicenza)
    {
        String tmp = "";
        String key = "";
        
        if (pivacf.length() < 16)
            pivacf = funzStringa.stringa("0", 16 - pivacf.length()) + pivacf;
        // mischia la partita iva
        String p1 = pivacf.substring(0, 2);
        String p2 = pivacf.substring(2, 4);
        String p3 = pivacf.substring(4, 6);
        String p4 = pivacf.substring(6, 8);
        String p5 = pivacf.substring(8, 10);
        String p6 = pivacf.substring(10, 12);
        String p7 = pivacf.substring(12, 14);
        String p8 = pivacf.substring(14, 16);
        String singolaaz = "R";
        if (!singolaazienda)
            singolaaz = "M";
        tmp = p2 + p5 + p1 + singolaaz + p4 + p3 + p6 + p8 + p7;
        // tipoinst 0 = start,1 = full, 2 = erp, 3 = elite
        int ip1 = Formattazione.estraiIntero(p5);
        if (ip1 < 50)
        {
            if (tipoinst == 0)
                tmp += "A";
            else if (tipoinst == 1)
                tmp += "K";
            else if (tipoinst == 2)
                tmp += "W";
            else
                tmp += "F";
        }
        else
        {
            if (tipoinst == 0)
                tmp += "B";
            else if (tipoinst == 1)
                tmp += "Z";
            else if (tipoinst == 2)
                tmp += "H";
            else
                tmp += "Y";
        }
        tmp += nlicenza;
        // calcola un carattere di check finale
        int tc = 0;
        for (int i = 0; i < 17; i++)
        {
            tc += tmp.getBytes()[i];
        }
        tc = tc % 26;
        byte[] b = new byte[1];
        b[0] = (byte) (65 + tc);
        tmp += new String(b);
        //Debug.output(tmp);
        // cripta in MD5
        try
        {
            //System.out.println(" stringa orig "  + tmp);
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(tmp.getBytes());
            //KeyPairGenerator keygen = KeyPairGenerator.getInstance("DSA");
            //keygen.initialize(512, new SecureRandom(md.digest()));
            //KeyPair pair = keygen.generateKeyPair();
            //System.out.println(" Public-key:  " + pair.getPublic().getEncoded()+ " Private-key: " + pair.getPrivate().getEncoded());
            //System.out.println(" Public-key:  " + pair.getPublic().getEncoded()+ " Private-key: " + pair.getPrivate().getEncoded());
            byte[] b2 = md.digest();
            
            //System.out.println(" pippo " + (new String(b2))  + " lung " + b2.length);
            
            for (int i = 0; i <b2.length; i++)
            {
                int val = b2[i];
                if (val < 0)
                {
                    val = val + 128;
                }
                key += funzStringa.riempiStringa(Integer.toHexString(val).toUpperCase(), 2, funzStringa.DX, '0');
            }
            //System.out.println(" lengthby "  + b2.length + "  lu key " + key.length());
           // System.out.println(key);
          
        }
        catch (Exception e)
        {
        }
        return key;
    }

    public static final String calcoloChiaveAttivazioneNet(String ragsoc, String nlicenza, int npos , boolean plugin)
    {
        String tmp = "";
        String key = "";
        
        String start = ragsoc + npos + nlicenza;
        if (start.length() < 70)
            start = funzStringa.riempiStringa(start, 70, funzStringa.SX, '0');
        else if (start.length() > 70)
            start = start.substring(0, 70);
        String p1 = start.substring(0, 7);
        String p2 = start.substring(7, 14);
        String p3 = start.substring(14, 21);
        String p4 = start.substring(21, 28);
        String p5 = start.substring(28, 35);
        String p6 = start.substring(35, 42);
        String p7 = start.substring(42, 49);
        String p8 = start.substring(49, 56);
        String p9 = start.substring(56, 63);
        String p10 = start.substring(63, 70);
        tmp = p2 + p5 + p1 + p4 + p3 + p6 + p8 + p7 + p10 + p9;
        
        if (!plugin)
        {
            String pl = tmp.substring(1,2);
            pl = "0";
            tmp = tmp.substring(0,1) + pl + tmp.substring(2); 
        }
        
        // calcola un carattere di check finale
        int tc = 0;
        for (int i = 0; i < 70; i++)
        {
            tc += tmp.getBytes()[i];
        }
        tc = tc % 26;
        byte[] b = new byte[1];
        b[0] = (byte) (65 + tc);
        tmp += new String(b);
        // cripta in MD5
        try
        {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(tmp.getBytes());
            byte[] b2 = md.digest();
            for (int i = 0; i <b2.length; i++)
            {
                int val = b2[i];
                if (val < 0)
                {
                    val = val + 128;
                }
                key += funzStringa.riempiStringa(Integer.toHexString(val).toUpperCase(), 2, funzStringa.DX, '0');
            }
        }
        catch (Exception e)
        {
        }
        return key;
    }
    
    //**********************  chiave di attivazione Accise
	public static final String calcoloChiaveAttivazioneMultiAccisa(String pivacf, int tipoinst, int singolalicenza, String nlicenza)
    {
        String tmp = "";
        String key = "";
        
        if (pivacf.length() < 16)
            pivacf = funzStringa.stringa("0", 16 - pivacf.length()) + pivacf;
        // mischia la partita iva
        String p1 = pivacf.substring(0, 2);
        String p2 = pivacf.substring(2, 4);
        String p3 = pivacf.substring(4, 6);
        String p4 = pivacf.substring(6, 8);
        String p5 = pivacf.substring(8, 10);
        String p6 = pivacf.substring(10, 12);
        String p7 = pivacf.substring(12, 14);
        String p8 = pivacf.substring(14, 16);
		String singolaaz = "R";
		switch(singolalicenza)
		{
		case 0 :singolaaz = "R";
		        break;
		case 1 :singolaaz = "M";
		        break;
		case 2 :singolaaz = "Z";
		        break;
		case 3 :singolaaz = "K";
		        break;
		case 4 :singolaaz = "Y";
		        break;
		case 5 :singolaaz = "A";
		        break;
		}
        
        //if (!singolaazienda)
        //    singolaaz = "M";
        tmp = p2 + p5 + p1 + singolaaz + p4 + p3 + p6 + p8 + p7;
        // tipoinst 0 = start,1 = full, 2 = erp, 3 = elite
        int ip1 = Formattazione.estraiIntero(p5);
        if (ip1 < 50)
        {
            if (tipoinst == 0)
                tmp += "A";
            else if (tipoinst == 1)
                tmp += "K";
            else if (tipoinst == 2)
                tmp += "W";
            else
                tmp += "F";
        }
        else
        {
            if (tipoinst == 0)
                tmp += "B";
            else if (tipoinst == 1)
                tmp += "Z";
            else if (tipoinst == 2)
                tmp += "H";
            else
                tmp += "Y";
        }
        tmp += nlicenza;
        // calcola un carattere di check finale
        int tc = 0;
        for (int i = 0; i < 17; i++)
        {
            tc += tmp.getBytes()[i];
        }
        tc = tc % 26;
        byte[] b = new byte[1];
        b[0] = (byte) (65 + tc);
        tmp += new String(b);
        //Debug.output(tmp);
        // cripta in MD5
        try
        {
            //System.out.println(" stringa orig "  + tmp);
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(tmp.getBytes());
            //KeyPairGenerator keygen = KeyPairGenerator.getInstance("DSA");
            //keygen.initialize(512, new SecureRandom(md.digest()));
            //KeyPair pair = keygen.generateKeyPair();
            //System.out.println(" Public-key:  " + pair.getPublic().getEncoded()+ " Private-key: " + pair.getPrivate().getEncoded());
            //System.out.println(" Public-key:  " + pair.getPublic().getEncoded()+ " Private-key: " + pair.getPrivate().getEncoded());
            byte[] b2 = md.digest();
            
            //System.out.println(" pippo " + (new String(b2))  + " lung " + b2.length);
            
            for (int i = 0; i <b2.length; i++)
            {
                int val = b2[i];
                if (val < 0)
                {
                    val = val + 128;
                }
                key += funzStringa.riempiStringa(Integer.toHexString(val).toUpperCase(), 2, funzStringa.DX, '0');
            }
            //System.out.println(" lengthby "  + b2.length + "  lu key " + key.length());
            //System.out.println(key);
          
        }
        catch (Exception e)
        {
        }
        return key;
    }
	//   fine chiave attivazione Accise

    public static final String calcoloChiaveAttivazioneMobile(String pivacf, String codterm, String nlicenza)
    {
        String tmp = "";
        String key = "";
        
        if (pivacf.length() < 16)
            pivacf = funzStringa.stringa("0", 16 - pivacf.length()) + pivacf;
        // mischia la partita iva
        String p1 = pivacf.substring(0, 2);
        String p2 = pivacf.substring(2, 4);
        String p3 = pivacf.substring(4, 6);
        String p4 = pivacf.substring(6, 8);
        String p5 = pivacf.substring(8, 10);
        String p6 = pivacf.substring(10, 12);
        String p7 = pivacf.substring(12, 14);
        String p8 = pivacf.substring(14, 16);
        
        // aggiunta codice terminale e licenza
        tmp = p2 + p5 + p1 + codterm + p4 + p3 + p6 + p8 + p7;
        tmp += nlicenza;
        // calcola un carattere di check finale
        int tc = 0;
        for (int i = 1; i < tmp.length(); i++)
        {
            //System.out.println("tc " + i + " " + (int) tmp.getBytes()[i]);
            tc += tmp.getBytes()[i];
        }
        tc = tc % 26;
        byte[] b = new byte[1];
        b[0] = (byte) (65 + tc);
        tmp += new String(b);
        //System.out.println(tmp);
        // cripta in MD5
        try
        {
            //System.out.println(" stringa orig "  + tmp);
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(tmp.getBytes());
            KeyPairGenerator keygen = KeyPairGenerator.getInstance("DSA");
            keygen.initialize(512, new SecureRandom(md.digest()));
            KeyPair pair = keygen.generateKeyPair();
            //System.out.println(" Public-key:  " + pair.getPublic().getEncoded()+ " Private-key: " + pair.getPrivate().getEncoded());
            byte[] b2 = md.digest();
            
            //System.out.println(" pippo " + (new String(b2))  + " lung " + b2.length);
            
            for (int i = 0; i <b2.length; i++)
            {
                int val = b2[i];
                if (val < 0)
                {
                    val = val + 128;
                }
                key += funzStringa.riempiStringa(Integer.toHexString(val).toUpperCase(), 2, funzStringa.DX, '0');
            }
            //System.out.println(" lengthby "  + b2.length + "  lu key " + key.length());
            //System.out.println(key);
             
        }
        catch (Exception e)
        {
        }
        return key;
    }

}
