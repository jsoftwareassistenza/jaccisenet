import java.io.*;

/**
 * Questa classe è utilizzata in associazione con la classe Record per incapsulare record di tabelle di Data Bases
 * 
 * @author Paolo Ginanneschi
 * @version 1.0
 */

public class Campo implements Serializable
{

    public static int STRING = 1;
    public static int SHORT = 2;
    public static int INT = 3;
    public static int LONG = 4;
    public static int FLOAT = 5;
    public static int DOUBLE = 6;
    public static int BOOLEAN = 7;
    public static int VERO = 10;
    public static int FALSO = 11;
    public static int DECIMAL = 12;
    
    /**
     * Nome campo su DB
     */
    private String nome;

    /**
     * Oggetto che contiene il valore
     */
    private Object val;

    /**
     * tipo dato
     */
    private int tipo;

    /**
     * Costruttore della classe
     * 
     * @param n Nome campo
     * @param v Oggetto che incapsula il valore del campo
     */
    public Campo(String n, Object v, int tipo)
    {
        this.nome = n;
        this.val = v;
        this.tipo = tipo;
    
		//{{INIT_CONTROLS
		//}}
	}

    /**
     * Restituisce l'oggetto valore associato al campo
     * 
     * @return Oggetto valore
     */

    public Object valore()
    {
        return this.val;
    }        

    /**
     * Restituisce il nome del campo
     * 
     * @return Stringa nome
     */
    
    public String nome()
    {
        return this.nome;
    }        

    /**
     * Restituisce il tipo JAVA del valore del campo (es.
     * stringa di testo -> java.lang.String)
     * 
     * @return Stringa denominazione tipo
     */

    public int tipo()
    {
        return tipo;
    }        
		//{{DECLARE_CONTROLS
	//}}
}
