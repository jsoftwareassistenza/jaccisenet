/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * DialogOperatoriJDaa.java
 *
 * Created on 16-feb-2010, 14.34.45
 */

/** 
 *
 * @author ginannaschi
 */
import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import java.sql.*;
import java.awt.*;
import java.util.*;
import java.lang.*;

public class DialogOperatoriJAcciseNet extends javax.swing.JDialog {

    private Connection conn = null;
    private int[] vid = null;
    private String azienda = "";
    private int idcorr = -1;
    private boolean ldap = false;

    /** Creates new form DialogOperatoriJDaa */
    public DialogOperatoriJAcciseNet(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    public DialogOperatoriJAcciseNet(java.awt.Frame parent, boolean modal, Connection conn) {
        this(parent, modal);
        this.conn = conn;
    }
    
    public DialogOperatoriJAcciseNet(java.awt.Frame parent, boolean modal, Connection conn,boolean ldap) {
        this(parent, modal);
        this.conn = conn;
        this.ldap = ldap;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        documentoTesto1 = new halleytech.models.DocumentoTesto();
        documentoTesto2 = new halleytech.models.DocumentoTesto();
        documentoTesto3 = new halleytech.models.DocumentoTesto();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        pScroll = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        lb_pwd2 = new javax.swing.JLabel();
        opusername = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        newpassword = new javax.swing.JPasswordField();
        buttonIns = new halleylib.beans.NftButton();
        buttonCanc = new halleylib.beans.NftButton();
        oppassword = new javax.swing.JPasswordField();
        label_pwd = new javax.swing.JLabel();
        lb_pwd1 = new javax.swing.JLabel();
        opz_tutte = new javax.swing.JRadioButton();
        opz_sel = new javax.swing.JRadioButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabpos = new javax.swing.JTable();

        documentoTesto1.scriviLung(50);

        documentoTesto2.scriviLung(8);

        documentoTesto3.scriviLung(8);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("JACCISE-NET - Gestione operatori");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 11)); // NOI18N
        jLabel1.setText("Elenco operatori");
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        getContentPane().add(jLabel1);
        jLabel1.setBounds(10, 0, 396, 20);

        tab.setBackground(new java.awt.Color(255, 255, 232));
        tab.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        tab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMouseClicked(evt);
            }
        });
        pScroll.setViewportView(tab);

        getContentPane().add(pScroll);
        pScroll.setBounds(10, 20, 530, 220);

        lb_pwd2.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        lb_pwd2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lb_pwd2.setText("nuova password");
        getContentPane().add(lb_pwd2);
        lb_pwd2.setBounds(190, 270, 90, 20);

        opusername.setDocument(documentoTesto1);
        opusername.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        opusername.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                opusernameFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                opusernameFocusLost(evt);
            }
        });
        getContentPane().add(opusername);
        opusername.setBounds(90, 250, 280, 20);

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Nome utente");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(20, 250, 70, 20);

        newpassword.setDocument(documentoTesto3);
        newpassword.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                newpasswordFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                newpasswordFocusLost(evt);
            }
        });
        getContentPane().add(newpassword);
        newpassword.setBounds(280, 270, 90, 20);

        buttonIns.setBackground(new java.awt.Color(182, 181, 193));
        buttonIns.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/png/16x16/save.png"))); // NOI18N
        buttonIns.setText("INS");
        buttonIns.setToolTipText("Inserimento/variazione registrazione");
        buttonIns.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        buttonIns.setOpaque(false);
        buttonIns.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonInsActionPerformed(evt);
            }
        });
        getContentPane().add(buttonIns);
        buttonIns.setBounds(380, 250, 160, 40);

        buttonCanc.setBackground(new java.awt.Color(182, 181, 193));
        buttonCanc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/png/16x16/trash.png"))); // NOI18N
        buttonCanc.setText("CANC");
        buttonCanc.setToolTipText("Cancellazione registrazione");
        buttonCanc.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        buttonCanc.setOpaque(false);
        buttonCanc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCancActionPerformed(evt);
            }
        });
        getContentPane().add(buttonCanc);
        buttonCanc.setBounds(380, 290, 160, 40);

        oppassword.setDocument(documentoTesto2);
        oppassword.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                oppasswordFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                oppasswordFocusLost(evt);
            }
        });
        getContentPane().add(oppassword);
        oppassword.setBounds(90, 270, 90, 20);

        label_pwd.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        label_pwd.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label_pwd.setText("Posizioni fiscali gestibili dall'operatore:");
        getContentPane().add(label_pwd);
        label_pwd.setBounds(10, 300, 220, 20);

        lb_pwd1.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        lb_pwd1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lb_pwd1.setText("Password");
        getContentPane().add(lb_pwd1);
        lb_pwd1.setBounds(0, 270, 90, 20);

        buttonGroup1.add(opz_tutte);
        opz_tutte.setText("Tutte");
        getContentPane().add(opz_tutte);
        opz_tutte.setBounds(10, 320, 59, 20);

        buttonGroup1.add(opz_sel);
        opz_sel.setText("Selezione:");
        getContentPane().add(opz_sel);
        opz_sel.setBounds(80, 320, 89, 20);

        tabpos.setBackground(new java.awt.Color(255, 255, 232));
        jScrollPane1.setViewportView(tabpos);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(80, 340, 460, 140);

        setSize(new java.awt.Dimension(566, 519));
    }// </editor-fold>//GEN-END:initComponents

    private void tabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMouseClicked
        if (tab.getSelectedRow() >= 0 && evt.getClickCount() == 2) {
            String ope = (String) tab.getValueAt(tab.getSelectedRow(), 0);
//            if (ope.equals("user"))
//            {
//                JOptionPane.showMessageDialog(this, "Operatore non modificabile",
//                    "Errore",JOptionPane.ERROR_MESSAGE);
//                return;
//            }
            int id = vid[tab.getSelectedRow()];
            QueryInterrogazione q = new QueryInterrogazione(conn, "SELECT * FROM operatori WHERE opid = " + id);
            q.apertura();
            if (q.numeroRecord() > 0) {
                Record r = q.successivo();
                idcorr = r.leggiIntero("opid");
                opusername.setText(r.leggiStringa("opusername"));
                lb_pwd2.setVisible(true);
                newpassword.setVisible(true);
                label_pwd.setText("Vecchia password");
                if (r.leggiIntero("opaccesso") == 0)
                    opz_tutte.setSelected(true);
                else
                    opz_sel.setSelected(true);
                buttonIns.setText("VAR");
                this.mostraPosizioniOperatore(r.leggiStringa("opusername"));
                opusername.requestFocus();
            }
            q.chiusura();
        }
}//GEN-LAST:event_tabMouseClicked

    private void opusernameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_opusernameFocusGained
}//GEN-LAST:event_opusernameFocusGained

    private void opusernameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_opusernameFocusLost
}//GEN-LAST:event_opusernameFocusLost

    private void buttonInsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonInsActionPerformed
        if (opusername.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this, "Specificare il nome utente",
                    "Errore",JOptionPane.ERROR_MESSAGE);
            opusername.requestFocus();
            return;
        }
        String pwd = new String(oppassword.getPassword());
        if (idcorr == -1 && !ldap)
        {
            if (pwd.equals("")) {
                JOptionPane.showMessageDialog(this, "Specificare la password",
                    "Errore",JOptionPane.ERROR_MESSAGE);
                newpassword.requestFocus();
                return;
            }
        }
        if (idcorr == -1) {
            QueryInterrogazione qes = new QueryInterrogazione(conn, "SELECT * FROM operatori WHERE opusername = '" + opusername.getText() + "'");
            qes.apertura();
            boolean esist = (qes.numeroRecord() > 0);
            qes.chiusura();
            if (esist) {
                JOptionPane.showMessageDialog(this, "Nome utente già esistente",
                    "Errore",JOptionPane.ERROR_MESSAGE);
                opusername.requestFocus();
                return;
            }
        }
        else
        {
            QueryInterrogazione qes = new QueryInterrogazione(conn, "SELECT * FROM operatori WHERE opusername = '" + opusername.getText() + "' AND opid <> " + idcorr);
            qes.apertura();
            boolean esist = (qes.numeroRecord() > 0);
            qes.chiusura();
            if (esist) {
                JOptionPane.showMessageDialog(this, "Nome utente già esistente",
                        "Errore",JOptionPane.ERROR_MESSAGE);
                opusername.requestFocus();
                return;
            }
        }

        if (idcorr != -1 && !ldap)
        {
            String newpwd = new String(newpassword.getPassword());
            if (!newpwd.equals("") && pwd.equals(""))
            {
                JOptionPane.showMessageDialog(this, "Per cambiare la password specificare prima la vecchia password",
                    "Errore",JOptionPane.ERROR_MESSAGE);
                oppassword.requestFocus();
                return;
            }
            if (!newpwd.equals("") && !pwd.equals(""))
            {
                QueryInterrogazione q = new QueryInterrogazione(conn, "SELECT oppassword FROM operatori WHERE opid = " + idcorr);
                q.apertura();
                String opwd = q.successivo().leggiStringa("oppassword");
                q.chiusura();
                if (!pwd.equals(opwd))
                {
                    JOptionPane.showMessageDialog(this, "La vecchia password non corrisponde",
                        "Errore",JOptionPane.ERROR_MESSAGE);
                    oppassword.requestFocus();
                    return;
                }
            }
            Record r = new Record();
            r.insElem("opusername", opusername.getText());
            if (!newpwd.equals("") && !pwd.equals(""))
                r.insElem("oppassword", newpwd);
            r.insElem("opstato", 1);
            r.insElem("opaccesso", (opz_tutte.isSelected()?0:1));
            r.insElem("opid", idcorr);
            QueryAggiornamento qagg = new QueryAggiornamento(conn, QueryAggiornamento.AGG, "operatori", "opid", r);
            qagg.esecuzione();
        }
        else
        {
            Record r = new Record();
            r.insElem("opusername", opusername.getText());
            r.insElem("oppassword", ldap?"":pwd);
            r.insElem("opaccesso", (opz_tutte.isSelected()?0:1));
            r.insElem("opstato", 1);
            QueryAggiornamento qins = new QueryAggiornamento(conn, QueryAggiornamento.INS, "operatori", "opid", r);
            qins.esecuzione();
            // visualizza solo le 10 notizie piu recenti
            QueryInterrogazione qn = new QueryInterrogazione(conn, "SELECT id FROM newsjdaa ORDER BY data DESC,ora DESC");
            qn.apertura();
            int nn = 0;
            for (int i = 0; i < qn.numeroRecord(); i++)
            {
                Record rn = qn.successivo();
                nn++;
                if (nn > 10)
                {
                    Record rx = new Record();
                    rx.insElem("opelogin", opusername.getText());
                    rx.insElem("newsid", rn.leggiIntero("id"));
                    QueryAggiornamento qinsn = new QueryAggiornamento(conn, QueryAggiornamento.INS, "lettura_newsjdaa", "id", rx);
                    qinsn.esecuzione();
                }
            }
            qn.chiusura();
        }
        // aggiorna posizioni
        QueryAggiornamento qd = new QueryAggiornamento(conn,
            "DELETE FROM operatori_pos WHERE osusername = '" + opusername.getText() + "'");
        qd.esecuzione();
        if (opz_sel.isSelected())
        {
            for (int i = 0; i < tabpos.getRowCount(); i++)
            {
                if (((Boolean) tabpos.getValueAt(i, 0)).booleanValue())
                {
                    String ca = (String) tabpos.getValueAt(i, 1);
                    String cu = (String) tabpos.getValueAt(i, 2);
                    String ti = (String) tabpos.getValueAt(i, 3);
                    Record rp = new Record();
                    rp.insElem("osusername", opusername.getText());
                    rp.insElem("oscodaccisa", ca);
                    rp.insElem("oscodute", cu);
                    rp.insElem("ostipopos", ti);
                    QueryAggiornamento qinsp = new QueryAggiornamento(conn, QueryAggiornamento.INS, "operatori_pos", "osid", rp);
                    qinsp.esecuzione();
                }
            }
        }
        this.mostralista();
        idcorr = -1;
        buttonIns.setText("INS");
        opusername.setText("");
        oppassword.setText("");
        newpassword.setText("");
        label_pwd.setText("Password");
        lb_pwd2.setVisible(false);
        newpassword.setVisible(false);
        opz_tutte.setSelected(true);
        buttonIns.setText("INS");
        this.mostraPosizioniOperatore("");
        opusername.requestFocus();
}//GEN-LAST:event_buttonInsActionPerformed

    private void buttonCancActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCancActionPerformed
        if (tab.getSelectedRow() >= 0) {
            String ope = (String) tab.getValueAt(tab.getSelectedRow(), 0);
//            if (ope.equals("user"))
//            {
//                JOptionPane.showMessageDialog(this, "Operatore non eliminabile",
//                    "Errore",JOptionPane.ERROR_MESSAGE);
//                return;
//            }
            int ris = JOptionPane.showConfirmDialog(this,"Confermi eliminazione operatore?",
                    "Conferma",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
            if (ris == JOptionPane.YES_OPTION) {
                QueryAggiornamento qd = new QueryAggiornamento(conn, "DELETE FROM operatori WHERE opid = " + vid[tab.getSelectedRow()]);
                qd.esecuzione();
                this.mostralista();
                buttonIns.setText("INS");
            }
        }
}//GEN-LAST:event_buttonCancActionPerformed

    private void newpasswordFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_newpasswordFocusGained
    }//GEN-LAST:event_newpasswordFocusGained

    private void newpasswordFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_newpasswordFocusLost
    }//GEN-LAST:event_newpasswordFocusLost

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        if (ldap)
        {
            lb_pwd1.setVisible(false);
            lb_pwd2.setVisible(false);
            oppassword.setVisible(false);
            newpassword.setVisible(false);
        }
        this.mostralista();
        lb_pwd2.setVisible(false);
        newpassword.setVisible(false);
        this.mostraPosizioniOperatore("");
        opz_tutte.setSelected(true);
        opusername.requestFocus();
    }//GEN-LAST:event_formWindowOpened

    private void oppasswordFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_oppasswordFocusGained
    }//GEN-LAST:event_oppasswordFocusGained

    private void oppasswordFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_oppasswordFocusLost
    }//GEN-LAST:event_oppasswordFocusLost

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private halleylib.beans.NftButton buttonCanc;
    private javax.swing.ButtonGroup buttonGroup1;
    private halleylib.beans.NftButton buttonIns;
    private halleytech.models.DocumentoTesto documentoTesto1;
    private halleytech.models.DocumentoTesto documentoTesto2;
    private halleytech.models.DocumentoTesto documentoTesto3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel label_pwd;
    private javax.swing.JLabel lb_pwd1;
    private javax.swing.JLabel lb_pwd2;
    private javax.swing.JPasswordField newpassword;
    private javax.swing.JPasswordField oppassword;
    private javax.swing.JTextField opusername;
    private javax.swing.JRadioButton opz_sel;
    private javax.swing.JRadioButton opz_tutte;
    private javax.swing.JScrollPane pScroll;
    private javax.swing.JTable tab;
    private javax.swing.JTable tabpos;
    // End of variables declaration//GEN-END:variables

    private void mostralista()
    {
        QueryInterrogazione qRic = null;
        String qry = "";
        Record r = null;

        qry = "SELECT * FROM operatori ORDER BY opusername";
        qRic = new QueryInterrogazione(conn, qry);
        qRic.apertura();
        int nRec = qRic.numeroRecord();
        // crea modello tabella
        final String[] names = {"Nome utente"};
        final Object[][] data = new Object[nRec][1];
        vid = new int[nRec];
        for (int i = 1; i <= nRec; i++)
        {
            r = qRic.successivo();
            data[i - 1][0] = r.leggiStringa("opusername");
            vid[i - 1] = r.leggiIntero("opid");
        }

        TableModel modello = new AbstractTableModel() {
            public int getColumnCount() { return names.length; }
            public int getRowCount() { return data.length;}
            public Object getValueAt(int row, int col) {return data[row][col];}
            public String getColumnName(int column) {return names[column];}
            public Class getColumnClass(int col) {return getValueAt(0,col).getClass();}
            public boolean isCellEditable(int row, int col) {
                    return false;}
            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        qRic.chiusura();

        tab.setModel(modello);
    }

    private void mostraPosizioniOperatore(String uname)
    {
        Vector vp = new Vector();
        QueryInterrogazione q = new QueryInterrogazione(conn, "SELECT * FROM codaccisa ORDER BY codiceaccisa,tipologia");
        q.apertura();
        final String[] names = {"", "C.Accisa", "C.utente", "Tipo"};
        final Object[][] data = new Object[q.numeroRecord()][4];
        for (int i = 0; i < q.numeroRecord(); i++)
        {
            Record r = q.successivo();
            boolean sel = false;
            QueryInterrogazione qes = new QueryInterrogazione(conn, "SELECT * FROM operatori_pos WHERE osusername = '" + uname + "' " +
                        " AND oscodaccisa = '" + r.leggiStringa("codiceaccisa") + "' AND oscodute = '" + r.leggiStringa("codiceutente") + "' AND " +
                        "ostipopos = '" + r.leggiStringa("tipologia") + "'");
            qes.apertura();
            if (qes.numeroRecord() > 0)
                sel = true;
            qes.chiusura();
            data[i][0] = new Boolean(sel);
            data[i][1] = r.leggiStringa("codiceaccisa");
            data[i][2] = r.leggiStringa("codiceutente");
            data[i][3] = r.leggiStringa("tipologia");
        }
        q.chiusura();
        TableModel modello = new AbstractTableModel() {
            public int getColumnCount() { return names.length; }
            public int getRowCount() { return data.length;}
            public Object getValueAt(int row, int col) {return data[row][col];}
            public String getColumnName(int column) {return names[column];}
            public Class getColumnClass(int col) {return getValueAt(0,col).getClass();}
            public boolean isCellEditable(int row, int col)
            {
                if (col == 0)
                    return true;
                else
                    return false;
            }
            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tabpos.setModel(modello);
    }
}
