
import java.io.*;
import java.util.*;
import java.sql.*;
import java.net.*;

/**
 * Questa classe permette di gestire interrogazioni di consultazione alla base dati
 * scelta, utilizzando stringhe SQL.
 * 
 * @author Paolo Ginanneschi
 * @see ConnessioneServer
 */



public class QueryInterrogazione
{

    /**
     * intero codice operazione dato dal server
     */
    private int codiceOperazione;

    /**
     * oggetto Connessione al server
     */
    private Connection cs = null;

    /**
     */
    private int numeroRec = -1;
    private String query = "";
    private boolean fine = false;
    private Statement st = null;
    private ResultSet rs = null;
    private boolean conteggio = true;

    /**
     * Costruttore: crea l'oggetto interrogazione
     * 
     * @param cs oggetto connessione al server
     * @param query stringa SQL
     */
    
    public QueryInterrogazione(Connection cs, String query)
    {
        this.cs = cs;
        this.query = query;
    
		//{{INIT_CONTROLS
		//}}
	}

    public QueryInterrogazione(Connection cs, String query, boolean numrec)
    {
        this.cs = cs;
        this.query = query;
        this.conteggio = numrec;
    
		//{{INIT_CONTROLS
		//}}
	}

    /**
     * restituisce il numero dei record restituiti dall'interrogazione
     * 
     * @return intero numero record
     */
        
    public int numeroRecord()
    {
        return numeroRec;
    }

    /**
     * apre l'interrogazione
     * 
     * @return Env.SUCCESSO o Env.FALLIMENTO
     */
    
    public int apertura()
    {
        int codice = Env.FALLIMENTO;
        try
        {
            //cs.checkConnessione();
            
            query = filtraStringaQuery(query);
            // se query standard calcola numero record
            String nrecqry = "";
            boolean count = true;
            if (conteggio)
            {
                if (query.toLowerCase().indexOf("distinct ") >= 0)
                {
                    if (query.toLowerCase().indexOf("\'", query.toLowerCase().indexOf("distinct ")) < 0 &&
                        query.toLowerCase().indexOf("\"", query.toLowerCase().indexOf("distinct ")) < 0)
                        count = false;
                }
                if (query.toLowerCase().indexOf("distinctrow ") >= 0)
                {
                    if (query.toLowerCase().indexOf("\'", query.toLowerCase().indexOf("distinctrow ")) < 0 &&
                        query.toLowerCase().indexOf("\"", query.toLowerCase().indexOf("distinctrow ")) < 0)
                        count = false;
                }
                if (query.toLowerCase().indexOf("group by") >= 0)
                {
                    int posgroup = 0;
                    while (query.toLowerCase().indexOf("group by", posgroup + 1) >= 0)
                    {
                        posgroup = query.toLowerCase().indexOf("group by", posgroup + 1);
                    }
                    if (query.toLowerCase().indexOf("\'", posgroup + 1) < 0 &&
                        query.toLowerCase().indexOf("\"", posgroup + 1) < 0)
                    {
                        count = false;
                    }
                }
                
                if (count)
                {
                    int posgroup = 0;
                    if (query.toLowerCase().indexOf("order by") >= 0)
                    {
                        while (query.toLowerCase().indexOf("order by", posgroup + 1) >= 0)
                        {
                            posgroup = query.toLowerCase().indexOf("order by", posgroup + 1);
                        }

                    }
                    
                    int posfrom = query.toLowerCase().indexOf("from ");
                    if (posfrom >= 0 && posgroup > 0)
                        nrecqry = "SELECT COUNT(*) " + query.substring(posfrom,posgroup);
                    else if (posfrom >= 0 && posgroup == 0)
                        nrecqry = "SELECT COUNT(*) " + query.substring(posfrom);
                    else
                        count = false;
                }
            }
            else
                count = false;
/*
            StringTokenizer s = new StringTokenizer(query);
            boolean count = true;
            Debug.output("TOKENS:");
            while (s.hasMoreElements())
            {
                String parte = (String) s.nextElement();
                Debug.output("   *" + parte + "*");
                if (parte.equalsIgnoreCase("distinct") || parte.equalsIgnoreCase("distinctrow") || parte.equalsIgnoreCase("group"))
                {
                    count = false;
                    break;
                }
                if (count)
                {
                    if (parte.equalsIgnoreCase("select"))
                    {
                        nrecqry += parte + " ";
                        nrecqry += "COUNT(*) ";
                        // cerca from 
                        boolean trovatoFrom = false;
                        while (!trovatoFrom)
                        {
                            String f = (String) s.nextElement();
                            if (f.equalsIgnoreCase("from"))
                                trovatoFrom = true;
                        }
                        nrecqry += "FROM ";
                    }
                    else
                        nrecqry += parte + " ";
                }
            }
*/
            if (!count)
                numeroRec = -1;
            else
            {
                try
                {
                    Statement cst = cs.createStatement();
                    ResultSet crs = cst.executeQuery(nrecqry);
                    crs.next();
                    numeroRec = crs.getInt(1);
                    crs.close();
                    cst.close();
                }
                catch (Exception e2)
                {
                    System.out.println("Errore interrogazione: " + e2.getMessage());
                }
            }
            // apre query
            
            try
            {
                st = cs.createStatement();
                rs = st.executeQuery(query);
            }
            catch (Exception e4)
            {
                System.out.println("Errore interrogazione: " + e4.getMessage());
            }
            codice = Env.SUCCESSO;
        }
        catch (Exception e)
        {
            System.out.println("Errore interrogazioni: " + e.getMessage());
            return Env.FALLIMENTO;
        }            
        return codice;
    }       

    /**
     * restituisce il record successivo
     * (chiamata la prima volta per posizionarsi
     * al primo record)
     * 
     * @return oggetto Record
     */
 
    public String query()
    {
        return query;
    }
    
    public Record successivo()
    {
        Record r = null;
        try
        {
            if (rs.next())
                r = new Record(rs);
            else
                r = null;
        }
        catch (SQLException e)
        {
            System.out.println("Errore successivo query: " + e.getMessage());
            r = null;
        }
        return r;
    }

    /**
     * chiude l'interrogazione
     */

    public boolean chiusura()
    {
        boolean esito = true;
        try
        {
            rs.close();
            st.close();
        }
        catch (Exception e)
        {
            System.out.println("Errore chiusura query: " + e.getMessage());
            return false;
        }            
        return esito;
    }
		//{{DECLARE_CONTROLS
	//}}

    public static final String filtraStringaQuery(String qry)
    {
        String ris = "";
        int i = 0;
        int pos = 0;
        int posfine = 0;
        boolean ok = false;
            
        while(i < qry.length())
        {
            if (qry.charAt(i) == '\"')
            {
                ris = ris + "\'";
                pos = i + 1;
                ok = false;
                while (!ok)
                {
                    posfine = qry.indexOf("\"", pos);
                    if (qry.charAt(posfine - 1) != '\\')
                    ok = true;
                }                         
                for (int j = i + 1; j < posfine /*qry.indexOf("\" ", pos)*/; j++)
                {
                    if (qry.charAt(j) == '\'')
                        ris = ris + Env.esc + '\'';
                    else
                        ris = ris + qry.charAt(j);
                }
                ris = ris + "\'";
                i = posfine;
            }
            else
                ris = ris + qry.charAt(i);
            i++;
        }
        return ris;
    }
}