/*
 * ProcessoControlloFTP.java
 *
 * Created on 6 aprile 2009, 14.33
 */

import java.sql.*;
import java.util.*;
import java.awt.*;
import java.lang.*;
import java.io.*;
import java.text.*;

/**
 *
 * @author  ginannaschi
 */
public class ProcessoControlloCartellaCondivisa extends Thread
{
    public String pcc = "";
    public long intervallo = 5000;
    public String percorso_copia = "";
    public String percorso_storico = "";
    public boolean uscita = false;
    private Connection conn2 = null;
    
    public ProcessoControlloCartellaCondivisa(String pcc, long intervallo, String percorso_copia, String percorso_storico)
    {
        this.pcc = pcc;
        this.percorso_copia = percorso_copia;
        this.percorso_storico = percorso_storico;
        this.intervallo = intervallo;
    }
    
    public void run()
    {
        try
        {
            conn2 = DriverManager.getConnection("jdbc:derby://localhost:" + EnvJAcciseNet.portadb + "/jazzaccise_server");
        }
        catch (Exception e)
        {
        }
        while (!uscita)
        {
            //System.out.println("uscita: " + uscita);
            try
            {
                //System.out.println("Controllo cartella condivisa in corso...");
                File f = new File(pcc);
                Statement st = conn2.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM codaccisa");
                while (rs.next())
                {
                    String codacc = rs.getString("codiceaccisa");
                    //System.out.println("controllo " + codacc + rs.getString("tipologia").trim());
                    // cerca files su cartella con codice accisa corrente
                    File facc = new File(pcc + File.separator + codacc + rs.getString("tipologia").trim());
                    if (facc.exists() && facc.isDirectory())
                    {
                        String[] files = facc.list();
                        if (files.length > 0)
                            System.out.println( "N.files in dir " + codacc + rs.getString("tipologia").trim() + ": " + files.length);
                        // copia files
                        for (int i = 0; i < files.length; i++)
                        {
                            //svuota cartella temporanea
                            File ftmp = new File("tmp" + codacc + rs.getString("tipologia").trim());
                            if (!ftmp.exists())
                                ftmp.mkdir();
                            ftmp = new File("tmp" + codacc + rs.getString("tipologia").trim());
                            String[] lftmp = ftmp.list();
                            for (int j = 0; j < lftmp.length; j++)
                            {
                                File fd = new File("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + lftmp[j]);
                                fd.delete();
                            }
                            this.sleep(3000);
                            // scarica file su cartella temporanea
                            File ff = new File(pcc + File.separator + codacc + rs.getString("tipologia").trim() + File.separator + files[i]);
                            System.out.println("download " + files[i] + " in corso...");
                            boolean copiaok = EnvJAcciseNet.copiaFile(pcc + File.separator + codacc + rs.getString("tipologia").trim() + File.separator + files[i],
                                "tmp" + codacc + rs.getString("tipologia").trim() + File.separator + files[i]);
                            System.out.println("copiaok:" + copiaok);
                            System.out.println("download " + files[i] + " ok");
                            boolean downloadOk = true;
                            if (downloadOk)
                            {
                                // controllo integrità file
                                boolean fileok = false;
                                try
                                {
                                    RandomAccessFile in = new RandomAccessFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + files[i], "r");
                                    String linea = in.readLine();
                                    if (linea != null)
                                    {
                                        linea = funzStringa.rimuovi(linea, "\r");
                                        linea = funzStringa.rimuovi(linea, "\n");
                                        if (linea.length() == 75)
                                        {
                                            String nfi = linea.substring(16, 28);
                                            if (nfi.equalsIgnoreCase(files[i]))
                                                fileok = true;
                                        }
                                    }
                                    in.close();
                                }
                                catch (Exception ectr)
                                {
                                    ectr.printStackTrace();
                                }
                                if (fileok)
                                {
                                    System.out.println("controllo: file " + files[i] + " ok");
                                    // cancella file da cartella condivisa
                                    ff.delete();
                                    // controllo esistenza file del giorno con stesso nome gi� archiviato
                                    File fes = new File(percorso_copia + File.separator + files[i]);
                                    if (fes.exists())
                                    {
                                        System.out.println("file gia presente...");
                                        // controllo con stesso codice utente ma codice accisa diverso    
                                        // prelievo codice accisa da file gi� archiviato
                                        String oldca = "";
                                        String oldtip = "";
                                        try
                                        {
                                            RandomAccessFile in = new RandomAccessFile(percorso_copia + File.separator + files[i], "r");
                                            String l = in.readLine();
                                            if (l != null)
                                            {
                                                l = in.readLine();
                                                if (l != null && l.length() > 20)
                                                {
                                                    oldca = l.substring(6, 19);
                                                    oldtip = l.substring(0, 6);
                                                }
                                            }
                                            in.close();
                                        }
                                        catch (Exception eca)
                                        {
                                        }
                                        System.out.println("codice accisa file presente:" + oldca);
                                        System.out.println("tipologia file presente:" + oldtip);
                                        // prelievo codice accisa da file appena arrivato
                                        String newca = "";
                                        String newtip = "";
                                        try
                                        {
                                            RandomAccessFile in = new RandomAccessFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + files[i], "r");
                                            String l = in.readLine();
                                            if (l != null)
                                            {
                                                l = in.readLine();
                                                if (l != null && l.length() > 20)
                                                {
                                                    newca = l.substring(6, 19);
                                                    newtip = l.substring(0, 6);
                                                }
                                            }
                                            in.close();
                                        }
                                        catch (Exception eca)
                                        {
                                        }
                                        System.out.println("codice accisa nuovo file:" + newca);
                                        System.out.println("tipologia nuovo file:" + newtip);
                                        if ((!oldca.equals("") && !newca.equalsIgnoreCase("") && !newca.equalsIgnoreCase(oldca)) ||
                                            (!oldtip.equals("") && !newtip.equalsIgnoreCase("") && !newtip.equalsIgnoreCase(oldtip)))
                                        {
                                            System.out.println("aggiornamento progressivo nuovo file...");
                                            // aggiorna nome file
                                            int prog = Formattazione.estraiIntero(files[i].substring(files[i].length() - 2));
                                            boolean ok = false;
                                            while (!ok)
                                            {
                                                prog++;
                                                File fconfr = new File(percorso_copia + File.separator + files[i].substring(0, files[i].length() - 2) + 
                                                    Formattazione.formIntero(prog, 2, "0"));
                                                if (!fconfr.exists())
                                                {
                                                    ok = true;
                                                }
                                            }
                                            System.out.println("nuovo progressivo:" + prog);
                                            File fnew = new File("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + files[i]);
                                            String newnome = files[i].substring(0, files[i].length() - 2) +
                                                Formattazione.formIntero(prog, 2, "0");
                                            fnew.renameTo(new File("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + newnome));
                                            FileOutputStream fos2 = new FileOutputStream("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + newnome + "x");
                                            // aggiorna nome file dentro al file
                                            RandomAccessFile in = new RandomAccessFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + newnome, "r");
                                            String lold = in.readLine();
                                            lold = lold.substring(0, 26) + Formattazione.formIntero(prog, 2, "0") + lold.substring(28);
                                            fos2.write((lold + "\r\n").getBytes());
                                            lold = in.readLine();
                                            while (lold != null)
                                            {
                                                fos2.write((lold + "\r\n").getBytes());
                                                lold = in.readLine();
                                            }
                                            fos2.close();
                                            in.close();
                                            File fd = new File("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + newnome);
                                            fd.delete();
                                            File fx = new File("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + newnome + "x");
                                            File fx2 = new File("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + newnome);
                                            fx.renameTo(fx2);
                                            // copia su cartella files da firmare
                                            EnvJAcciseNet.copiaFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + newnome, percorso_copia + File.separator + newnome);
                                            if (!percorso_storico.equals(""))
                                            {
                                                // copia su cartella storico files da firmare
                                                EnvJAcciseNet.copiaFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + newnome, percorso_storico + File.separator + newnome);
                                            }
                                            // registra su db in tabella files_in
                                            PreparedStatement pst = conn2.prepareStatement(
                                                "INSERT INTO files_in (nomefile,codutente,codiceaccisa,tipologia,dataarrivo,oraarrivo) VALUES (?,?,?,?,?,?)");
                                            pst.clearParameters();
                                            pst.setString(1, newnome);
                                            pst.setString(2, newnome.substring(0, 4));
                                            pst.setString(3, codacc);
                                            pst.setString(4, rs.getString("tipologia"));
                                            pst.setString(5, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                                            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                                            pst.setString(6, df.format(new java.util.Date()));
                                            pst.execute();
                                            pst.close();
                                        }
                                        else
                                        {
                                            // copia su cartella files da firmare
                                            EnvJAcciseNet.copiaFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + files[i], percorso_copia + File.separator + files[i]);
                                            if (!percorso_storico.equals(""))
                                            {
                                                // copia su cartella storico files da firmare
                                                EnvJAcciseNet.copiaFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + files[i], percorso_storico + File.separator + files[i]);
                                            }
                                            // registra su db in tabella files_in
                                            PreparedStatement pst = conn2.prepareStatement(
                                                "INSERT INTO files_in (nomefile,codutente,codiceaccisa,tipologia,dataarrivo,oraarrivo) VALUES (?,?,?,?,?,?)");
                                            pst.clearParameters();
                                            pst.setString(1, files[i]);
                                            pst.setString(2, files[i].substring(0, 4));
                                            pst.setString(3, codacc);
                                            pst.setString(4, rs.getString("tipologia"));
                                            pst.setString(5, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                                            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                                            pst.setString(6, df.format(new java.util.Date()));
                                            pst.execute();
                                            pst.close();
                                        }
                                    }
                                    else
                                    {
                                        // copia su cartella files da firmare
                                        EnvJAcciseNet.copiaFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + files[i], percorso_copia + File.separator + files[i]);
                                        if (!percorso_storico.equals(""))
                                        {
                                            // copia su cartella storico files da firmare
                                            EnvJAcciseNet.copiaFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + files[i], percorso_storico + File.separator + files[i]);
                                        }
                                        // registra su db in tabella files_in
                                        PreparedStatement pst = conn2.prepareStatement(
                                            "INSERT INTO files_in (nomefile,codutente,codiceaccisa,tipologia,dataarrivo,oraarrivo) VALUES (?,?,?,?,?,?)");
                                        pst.clearParameters();
                                        pst.setString(1, files[i]);
                                        pst.setString(2, files[i].substring(0, 4));
                                        pst.setString(3, codacc);
                                        pst.setString(4, rs.getString("tipologia"));
                                        pst.setString(5, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                                        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                                        pst.setString(6, df.format(new java.util.Date()));
                                        pst.execute();
                                        pst.close();
                                    }
                                }
                                else
                                {
                                    System.out.println("controllo: file " + files[i] + " non coerente");    
                                    // cancella file da cartella condivisa
                                    ff.delete();
                                }
                            }
                            else
                            {
                                ff.delete();
                            }
                        }
                    }    
                    else
                    {
                        // crea la directory
                        boolean mkd = facc.mkdir();
                        if (mkd)
                        {
                            System.out.println("cartella condivisa: creata directory " + codacc + rs.getString("tipologia").trim());
                        }
                    }
                }
                st.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            try
            {
                this.sleep(intervallo);
            }
            catch (Exception esleep)
            {
            }
        }
        try
        {
            conn2.close();
        }
        catch (Exception ec)
        {
        }
    }
}
