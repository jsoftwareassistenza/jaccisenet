/*
 * ProcessoControlloFTP.java
 *
 * Created on 6 aprile 2009, 14.33
 */

import java.sql.*;
import java.util.*;
import java.awt.*;
import java.lang.*;
import java.io.*;
import java.text.*;

/**
 *
 * @author  ginannaschi
 */
public class ProcessoControlloEmailIn extends Thread
{
    public String pcc = "";
    public long intervallo = 5000;
    public String server_pop3 = "";
    public int porta_pop3 = 110;
    public String username = "";
    public String password = "";
    public boolean uscita = false;
    private Connection conn2 = null;
    
    public ProcessoControlloEmailIn(String pcc, long intervallo, String server_pop3, int porta_pop3, String username, String password)
    {
        this.pcc = pcc;
        this.server_pop3 = server_pop3;
        this.porta_pop3 = porta_pop3;
        this.username = username;
        this.password = password;
        this.intervallo = intervallo;
    }
    
    public void run()
    {
        try
        {
            conn2 = DriverManager.getConnection("jdbc:derby://localhost:" + EnvJAcciseNet.portadb + "/jazzaccise_server");
        }
        catch (Exception e)
        {
        }
        while (!uscita)
        {
            //System.out.println("uscita: " + uscita);
            try
            {
                //System.out.println("Controllo arrivo email in corso...");
                //svuota cartella temporanea
                File ftmp = new File("tmp_email_in");
                if (!ftmp.exists())
                    ftmp.mkdir();
                ftmp = new File("tmp_email_in");
                String[] lftmp = ftmp.list();
                for (int j = 0; j < lftmp.length; j++)
                {
                    File fd = new File("tmp_email_in" + File.separator + lftmp[j]);
                    fd.delete();
                }
                // prelievo allegati email e salvataggio su tmp_email_in e sottocartelle F<x>
                boolean ricok = FunzioniEmail.riceviEmail(server_pop3, porta_pop3, username, password, "tmp_email_in");
                // per ogni file controllo cod.accisa,cod.utente,tipologia e copia su cartella condivisa corrispondente
                ftmp = new File("tmp_email_in");
                for (int i = 1; i <= 100; i++)
                {
                    File ff = new File("tmp_email_in" + File.separator + "F" + i);
                    if (ff.exists() && ff.isDirectory())
                    {
                        String[] lf = ff.list();
                        if (lf.length > 0)
                        {
                            System.out.println("   email in:" + lf[0]);
                            // controllo integrità file
                            boolean fileok = false;
                            try
                            {
                                RandomAccessFile in = new RandomAccessFile("tmp_email_in" + File.separator + "F" + i +
                                    File.separator + lf[0], "r");
                                String linea = in.readLine();
                                if (linea != null)
                                {
                                    linea = funzStringa.rimuovi(linea, "\r");
                                    linea = funzStringa.rimuovi(linea, "\n");
                                    if (linea.length() == 75)
                                    {
                                        String nfi = linea.substring(16, 28);
                                        if (nfi.equalsIgnoreCase(lf[0]))
                                        fileok = true;
                                    }
                                }
                                in.close();
                            }
                            catch (Exception ectr)
                            {
                                ectr.printStackTrace();
                            }
                            if (fileok)
                            {
                                // prelievo codice accisa, tipologia, codice utente
                                try
                                {
                                    RandomAccessFile in = new RandomAccessFile("tmp_email_in" + File.separator + "F" + i +
                                        File.separator + lf[0], "r");
                                    String l = in.readLine();
                                    String codutente = l.substring(0, 4);
                                    l = in.readLine();
                                    String tipologia = l.substring(0, 6);
                                    String codaccisa = l.substring(6, 19);
                                    in.close();
                                    System.out.println("   cod.utente:" + codutente);
                                    System.out.println("   cod.accisa:" + codaccisa);
                                    System.out.println("   tipologia:" + tipologia);
                                    File fc = new File(pcc + File.separator + codaccisa + tipologia);
                                    if (!fc.exists())
                                        fc.mkdir();
                                    // aspetta che la cartella condivisa di destinazione sia vuota
                                    boolean dirVuota = false;
                                    while (!dirVuota)
                                    {
                                        File fv = new File(pcc + File.separator + codaccisa + tipologia);
                                        if (fv.list().length == 0)
                                            dirVuota = true;
                                    }
                                    EnvJAcciseNet.copiaFile("tmp_email_in" + File.separator + "F" + i + File.separator + lf[0], pcc + File.separator + codaccisa + tipologia + File.separator + lf[0]);
                                    File fd = new File("tmp_email_in" + File.separator + "F" + i + File.separator + lf[0]);
                                    fd.delete();
                                    // registra su db in tabella files_in
                                    PreparedStatement pst = conn2.prepareStatement(
                                        "INSERT INTO email_in (nomefile,codutente,codiceaccisa,tipologia,dataric,oraric) VALUES (?,?,?,?,?,?)");
                                    pst.clearParameters();
                                    pst.setString(1, lf[0]);
                                    pst.setString(2, codutente);
                                    pst.setString(3, codaccisa);
                                    pst.setString(4, tipologia);
                                    pst.setString(5, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                                    SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                                    pst.setString(6, df.format(new java.util.Date()));
                                    pst.execute();
                                    pst.close();
                                }
                                catch (Exception eca)
                                {
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            try
            {
                this.sleep(intervallo);
            }
            catch (Exception esleep)
            {
            }
        }
        try
        {
            conn2.close();
        }
        catch (Exception ec)
        {
        }
    }
}
