/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.security.cert.X509Certificate;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.CertificateException;

/**
 *
 * @author Giovanni
 */
public class DummyTrustmanager implements X509TrustManager {
    @Override
    public void checkClientTrusted(X509Certificate[] xcs, String string)  {
        // do nothing
    }

    public void checkServerTrusted(X509Certificate[] xcs, String string)  {
        // do nothing
    }

    public X509Certificate[] getAcceptedIssuers() {
        return new java.security.cert.X509Certificate[0];
    }
}