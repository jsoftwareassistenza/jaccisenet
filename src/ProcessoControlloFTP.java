/*
 * ProcessoControlloFTP.java
 *
 * Created on 6 aprile 2009, 14.33
 */

import java.sql.*;
import java.util.*;
import java.awt.*;
import java.lang.*;
import java.io.*;
import org.apache.commons.net.ftp.*;
import java.text.*;

/**
 *
 * @author  ginannaschi
 */
public class ProcessoControlloFTP extends Thread
{
    public String indserver = "";
    public String username = "";
    public String pwd = "";
    public long intervallo = 5000;
    public String percorso_copia = "";
    public String percorso_storico = "";
    public boolean uscita = false;
    private Connection conn2 = null;
    
    public ProcessoControlloFTP(String indserver, String username, String pwd, long intervallo, String percorso_copia, String percorso_storico)
    {
        this.indserver = indserver;
        this.username = username;
        this.pwd = pwd;
        this.percorso_copia = percorso_copia;
        this.percorso_storico = percorso_storico;
        this.intervallo = intervallo;
    }
    
    public void run()
    {
        try
        {
            conn2 = DriverManager.getConnection("jdbc:derby://localhost:" + EnvJAcciseNet.portadb + "/jazzaccise_server");
        }
        catch (Exception e)
        {
        }
        while (!uscita)
        {
            //System.out.println("uscita: " + uscita);
            try
            {
                // collegamento al server ftp
                //System.out.println("Connessione a server ftp in corso...");
                FTPClient ftp = new FTPClient();
                ftp.connect(indserver);
                ftp.login(username, pwd);
                ftp.setFileType(FTP.BINARY_FILE_TYPE);
                ftp.enterLocalPassiveMode();
                System.out.println("Connesso a server ftp");
                System.out.print(ftp.getReplyString());
                Statement st = conn2.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM codaccisa");
                while (rs.next())
                {
                    String codacc = rs.getString("codiceaccisa");
                    System.out.println("controllo " + codacc + rs.getString("tipologia").trim());
                    // cerca files su cartella con codice accisa corrente
                    boolean cd = ftp.changeWorkingDirectory(codacc + rs.getString("tipologia").trim());
                    if (cd)
                    {
                        FTPFile[] files = ftp.listFiles();
                        System.out.println( "N.files in dir " + codacc + rs.getString("tipologia").trim() + ": " + files.length);
                        // copia files
                        for (int i = 0; i < files.length; i++)
                        {
                            //svuota cartella temporanea
                            File ftmp = new File("tmp" + codacc + rs.getString("tipologia").trim());
                            if (!ftmp.exists())
                                ftmp.mkdir();
                            ftmp = new File("tmp" + codacc + rs.getString("tipologia").trim());
                            String[] lftmp = ftmp.list();
                            for (int j = 0; j < lftmp.length; j++)
                            {
                                File fd = new File("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + lftmp[j]);
                                fd.delete();
                            }
                            // scarica file su cartella temporanea
                            System.out.println("download " + files[i].getName() + " in corso...");
                            File file = new File("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + files[i].getName());
                            FileOutputStream fos = new FileOutputStream(file); 
                            ftp.retrieveFile(files[i].getName(), fos);
                            fos.close();
                            System.out.println("download " + files[i].getName() + " ok");
                            // controllo lunghezza file
                            long lftp = files[i].getSize();
                            File flocale = new File("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + files[i].getName());
                            long lloc = flocale.length();
                            if (lftp != lloc)
                            {
                                System.out.println("lftp:" + lftp + "  llocale:" + lloc + " download errato");
                            }
                            else
                            {
                                // controllo integrità file
                                boolean fileok = false;
                                try
                                {
                                    RandomAccessFile in = new RandomAccessFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + files[i].getName(), "r");
                                    String linea = in.readLine();
                                    if (linea != null)
                                    {
                                        linea = funzStringa.rimuovi(linea, "\r");
                                        linea = funzStringa.rimuovi(linea, "\n");
                                        if (linea.length() == 75)
                                        {
                                            String nfi = linea.substring(16, 28);
                                            if (nfi.equalsIgnoreCase(files[i].getName()))
                                                fileok = true;
                                        }
                                    }
                                    in.close();
                                }
                                catch (Exception ectr)
                                {
                                }
                                if (fileok)
                                {
                                    System.out.println("controllo: file " + files[i].getName() + " ok");
                                    ftp.deleteFile("/" + codacc + rs.getString("tipologia").trim() + "/" + files[i].getName());
                                    // controllo esistenza file del giorno con stesso nome già archiviato
                                    File fes = new File(percorso_copia + File.separator + files[i].getName());
                                    if (fes.exists())
                                    {
                                        System.out.println("file gia presente...");
                                        // controllo con stesso codice utente ma codice accisa diverso    
                                        // prelievo codice accisa da file già archiviato
                                        String oldca = "";
                                        String oldtip = "";
                                        try
                                        {
                                            RandomAccessFile in = new RandomAccessFile(percorso_copia + File.separator + files[i].getName(), "r");
                                            String l = in.readLine();
                                            if (l != null)
                                            {
                                                l = in.readLine();
                                                if (l != null && l.length() > 20)
                                                {
                                                    oldca = l.substring(6, 19);
                                                    oldtip = l.substring(0, 6);
                                                }
                                            }
                                            in.close();
                                        }
                                        catch (Exception eca)
                                        {
                                        }
                                        System.out.println("codice accisa file presente:" + oldca);
                                        System.out.println("tipologia file presente:" + oldtip);
                                        // prelievo codice accisa da file appena arrivato
                                        String newca = "";
                                        String newtip = "";
                                        try
                                        {
                                            RandomAccessFile in = new RandomAccessFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + files[i].getName(), "r");
                                            String l = in.readLine();
                                            if (l != null)
                                            {
                                                l = in.readLine();
                                                if (l != null && l.length() > 20)
                                                {
                                                    newca = l.substring(6, 19);
                                                    newtip = l.substring(0, 6);
                                                }
                                            }
                                            in.close();
                                        }
                                        catch (Exception eca)
                                        {
                                        }
                                        System.out.println("codice accisa nuovo file:" + newca);
                                        System.out.println("tipologia nuovo file:" + newtip);
                                        if ((!oldca.equals("") && !newca.equalsIgnoreCase("") && !newca.equalsIgnoreCase(oldca)) ||
                                            (!oldtip.equals("") && !newtip.equalsIgnoreCase("") && !newtip.equalsIgnoreCase(oldtip)))
                                        {
                                            System.out.println("aggiornamento progressivo nuovo file...");
                                            // aggiorna nome file
                                            int prog = Formattazione.estraiIntero(files[i].getName().substring(files[i].getName().length() - 2));
                                            boolean ok = false;
                                            while (!ok)
                                            {
                                                prog++;
                                                File fconfr = new File(percorso_copia + File.separator + files[i].getName().substring(0, files[i].getName().length() - 2) + 
                                                    Formattazione.formIntero(prog, 2, "0"));
                                                if (!fconfr.exists())
                                                {
                                                    ok = true;
                                                }
                                            }
                                            System.out.println("nuovo progressivo:" + prog);
                                            File fnew = new File("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + files[i].getName());
                                            String newnome = files[i].getName().substring(0, files[i].getName().length() - 2) +
                                                Formattazione.formIntero(prog, 2, "0");
                                            fnew.renameTo(new File("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + newnome));
                                            FileOutputStream fos2 = new FileOutputStream("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + newnome + "x");
                                            // aggiorna nome file dentro al file
                                            RandomAccessFile in = new RandomAccessFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + newnome, "r");
                                            String lold = in.readLine();
                                            lold = lold.substring(0, 26) + Formattazione.formIntero(prog, 2, "0") + lold.substring(28);
                                            fos2.write((lold + "\r\n").getBytes());
                                            lold = in.readLine();
                                            while (lold != null)
                                            {
                                                fos2.write((lold + "\r\n").getBytes());
                                                lold = in.readLine();
                                            }
                                            fos2.close();
                                            in.close();
                                            File fd = new File("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + newnome);
                                            fd.delete();
                                            File fx = new File("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + newnome + "x");
                                            File fx2 = new File("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + newnome);
                                            fx.renameTo(fx2);
                                            // copia su cartella files da firmare
                                            EnvJAcciseNet.copiaFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + newnome, percorso_copia + File.separator + newnome);
                                            if (!percorso_storico.equals(""))
                                            {
                                                // copia su cartella storico files da firmare
                                                EnvJAcciseNet.copiaFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + newnome, percorso_storico + File.separator + newnome);
                                            }
                                            // registra su db in tabella files_in
                                            PreparedStatement pst = conn2.prepareStatement(
                                                "INSERT INTO files_in (nomefile,codutente,codiceaccisa,tipologia,dataarrivo,oraarrivo) VALUES (?,?,?,?,?,?)");
                                            pst.clearParameters();
                                            pst.setString(1, newnome);
                                            pst.setString(2, newnome.substring(0, 4));
                                            pst.setString(3, codacc);
                                            pst.setString(4, rs.getString("tipologia"));
                                            pst.setString(5, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                                            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                                            pst.setString(6, df.format(new java.util.Date()));
                                            pst.execute();
                                            pst.close();
                                        }
                                        else
                                        {
                                            // copia su cartella files da firmare
                                            EnvJAcciseNet.copiaFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + files[i].getName(), percorso_copia + File.separator + files[i].getName());
                                            if (!percorso_storico.equals(""))
                                            {
                                                // copia su cartella storico files da firmare
                                                EnvJAcciseNet.copiaFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + files[i].getName(), percorso_storico + File.separator + files[i].getName());
                                            }
                                            // registra su db in tabella files_in
                                            PreparedStatement pst = conn2.prepareStatement(
                                                "INSERT INTO files_in (nomefile,codutente,codiceaccisa,tipologia,dataarrivo,oraarrivo) VALUES (?,?,?,?,?,?)");
                                            pst.clearParameters();
                                            pst.setString(1, files[i].getName());
                                            pst.setString(2, files[i].getName().substring(0, 4));
                                            pst.setString(3, codacc);
                                            pst.setString(4, rs.getString("tipologia"));
                                            pst.setString(5, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                                            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                                            pst.setString(6, df.format(new java.util.Date()));
                                            pst.execute();
                                            pst.close();
                                        }
                                    }
                                    else
                                    {
                                        // copia su cartella files da firmare
                                        EnvJAcciseNet.copiaFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + files[i].getName(), percorso_copia + File.separator + files[i].getName());
                                        if (!percorso_storico.equals(""))
                                        {
                                            // copia su cartella storico files da firmare
                                            EnvJAcciseNet.copiaFile("tmp" + codacc + rs.getString("tipologia").trim() + File.separator + files[i].getName(), percorso_storico + File.separator + files[i].getName());
                                        }
                                        // registra su db in tabella files_in
                                        PreparedStatement pst = conn2.prepareStatement(
                                            "INSERT INTO files_in (nomefile,codutente,codiceaccisa,tipologia,dataarrivo,oraarrivo) VALUES (?,?,?,?,?,?)");
                                        pst.clearParameters();
                                        pst.setString(1, files[i].getName());
                                        pst.setString(2, files[i].getName().substring(0, 4));
                                        pst.setString(3, codacc);
                                        pst.setString(4, rs.getString("tipologia"));
                                        pst.setString(5, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                                        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                                        pst.setString(6, df.format(new java.util.Date()));
                                        pst.execute();
                                        pst.close();
                                    }
                                }
                                else
                                {
                                    System.out.println("controllo: file " + files[i].getName() + " non coerente");    
                                }
                            }
                        }
                        ftp.changeToParentDirectory();
                    }    
                    else
                    {
                        // crea la directory
                        boolean mkd = ftp.makeDirectory(codacc + rs.getString("tipologia").trim());
                        if (mkd)
                        {
                            System.out.println("ftp: creata directory " + codacc + rs.getString("tipologia").trim());
                        }
                    }
                }
                st.close();
                ftp.logout();
                ftp.disconnect();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            try
            {
                this.sleep(intervallo);
            }
            catch (Exception esleep)
            {
            }
        }
        try
        {
            conn2.close();
        }
        catch (Exception ec)
        {
        }
    }
}
