/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyStore;
import java.util.Hashtable;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

/**
 *
 * @author Giovanni
 */
public class LdapsTls12 {

    /**
     * @param args the command line arguments
     */
    public static int CheckLDAPConnection(String user_name, String user_password, String url, String port, String domain) {
        try {
            Hashtable<String, String> env1 = new Hashtable<String, String>();

            env1.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            env1.put(Context.PROVIDER_URL, "ldaps://" + url + ":" + port);
//            env1.put(Context.PROVIDER_URL, "ldap://172.16.94.97:389");
            env1.put(Context.SECURITY_AUTHENTICATION, "simple");
            env1.put(Context.SECURITY_PRINCIPAL, user_name + "@" + domain);
//            env1.put(Context.SECURITY_PRINCIPAL, user_name + "@16.94");
            env1.put(Context.SECURITY_CREDENTIALS, user_password);
            env1.put(Context.SECURITY_PROTOCOL, "ssl");
            env1.put("java.naming.ldap.factory.socket", "DummySSLSocketFactory");
//            env1.put("java.naming.ldap.version", "3");

            try {
                //Connect with ldap
                InitialDirContext l = new InitialDirContext(env1);
                //Connection succeeded
                System.out.println("Connection succeeded!");
                return 0;
            } catch (AuthenticationException e) {
                //Connection failed
                System.out.println("Authentication failed!");
                e.printStackTrace();
                return 1;
            } catch (NamingException namEx) {
                System.out.println("LDAP connection failed!");
                namEx.printStackTrace();
                return 2;
            }
        } catch (Exception e) {
            System.out.println("LDAP Error");
            e.printStackTrace();
            return 3;
        }

    }


}
