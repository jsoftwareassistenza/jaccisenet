// Env Jazz

import java.awt.*;
import java.awt.print.*;
import java.sql.*;

public class Env {

    // tipi oggetto DB

    static final int TABELLADB = 1;
    static final int QUERYINTERROGAZIONEDB = 2;

    // messaggi oggetti DB
    static final int VUOTO = 0;
    static final int ELIMINAZIONE = 1;
    static final int INSERIMENTO = 2;
    static final int AGGIORNAMENTO = 3;

    // per maschere di scorrimento
    static final int INS = 0;
    static final int AGG = 1;
    static final int INSCHIAVE = 2;

    static final boolean INCREMENTO = true;
    static final boolean DECREMENTO = false;

    static final int NON_ESISTE = 0;
    static final int ESISTE = 1;

    static final int FALLIMENTO = 0;
    static final int SUCCESSO = 1;
    static final int DATIERRATI = 2;

    // parametri interfaccia
    static boolean BGEVID = true; // eidenziazione colorata casella
    static boolean SELEVID = true; // evidenziazione testo casella
    static Color TF_COLBGEVID = new Color(253, 240, 87); // colore dello sfondo evidenziato
    static Color TF_COLFGEVID = Color.black; // colore del testo evidenziato
    static Color TF_COLBG = Color.white; // colore sfondo normale
    static Color TF_COLFG = Color.black; // colore testo normale
    static Color COLSELEVID = new Color(204, 204, 255); // colore evidenziazione testo
    static Color KEY_COLBGEVID = Color.darkGray;
    static Color KEY_COLFGEVID = Color.yellow;

    public static final int RITORNO_RECORD = 0;
    public static final int TAB_STANDARD = 1;

    public static char dec;
    public static char errDec;

    // parametri tipo di operazione su tabelle
    public static final int VISUALIZZAZIONE = 0; // visualizza il record dell'id corrente
    public static final int NORMALE = 1; // edit standard
    public static final int RITORNO = 2; // uscendo dalla maschera passa l'id del salvato
    public static final int LETTURA = 3; // uscendo dalla maschera passa l'id del salvato
    // variabili globali per lo scambio tra maschere
    public static int idMsg;
    public static String[] msg = new String[10];
    public static double[] dmsg = new double[10];
    public static int[] imsg = new int[10];
    public static long[] lmsg = new long[10];

    // parametri stampa testo
    public static final int CPI10 = 0;
    public static final int CPI12 = 1;
    public static final int CPI17 = 2;

    public static final int LIRA = 1;
    public static final int EURO = 2;

    public int valutaLavoro = LIRA;

    public static final int STAMPANTE = 0;
    public static final int FILE = 1;
    public static final int ANTEPRIMA = 2;
    public static final int STAMPANTE_GRF = 3;
    public static final int EMAIL = 4;

    // variabili per gestione licenze
    public static boolean gestioneVino = true;
    public static boolean gestioneCauzioni = true;
    public static boolean gestioneConfGrano = true;
    public static boolean gestioneConfLatte = true;
    public static boolean gestioneConfVino = true;
    public static boolean gestionePennaOttica = true;
    public static boolean gestioneAzcom = true;

    public static boolean gestioneVinicola = true;
    public static boolean gestioneLatte = true;
    public static boolean gestioneAgricola = true;

    // variabili generiche di ambiente
    public static boolean prog_server = false;
    public static String output = "CONSOLE";
    public static boolean trem_invoc_client = true;
    public static int trem_pausa_unzip = 0;
    public static int trem_pausa_importdoc = 0;
    public static boolean debug_salvadoc = false;
    public static int pausa_bloccoelab = 3;
    public static boolean ope_controllo_ip = false;
    public static boolean stampa_giac_2 = false;
    public static boolean trem_blocco_trasm_inric = true;
    public static boolean usa_stesso_ip = true;
    public static boolean abilita_trasfrem = true;
    public static String ind_tabelle_stat = "";
    public static boolean trem_autostart = false;
    public static boolean trem_ric_gestincassi = false;
    public static boolean aggdb = true;
    public static boolean menu_esercizi = true;

    // pagina finale stampe
    public static int npagina = -1;

    public static int righemodulo = 66;

    // parametri stampe testo su stampanti grafiche
    public static boolean stampaTestoSuGrafica = false;
    public static String fontTesto = "Courier new";
    public static int grandFontTesto = 20;
    public static int righePerPaginaTesto = 66;
    public static double dimXPaginaTesto = 21;  // in Cm
    public static double dimYPaginaTesto = 29.7;  // in Cm
    public static double margineSxPaginaTesto = 2;  // in Cm
    public static double margineDxPaginaTesto = 2;  // in Cm
    public static double margineSupPaginaTesto = 2; // in Cm
    public static double margineInfPaginaTesto = 2; // in Cm
    public static double interlineaTesto = 0.5;     // in Cm
    public static int orientamentoFoglioTesto = 0;

    public static String fontTestoOrizz = "Courier new";
    public static int grandFontTestoOrizz = 20;
    public static int righePerPaginaTestoOrizz = 66;
    public static double interlineaTestoOrizz = 0.5;     // in Cm

    public static boolean usaEditor = false;
    public static String percorsoFiles = "";

    public static PrinterJob pj = null;
    public static PrintJob pjold = null;

    // numero aggiornamento o versione
    public static long numagg = 0;

    public static char esc = '\\';

    
    public Env() {

		//{{INIT_CONTROLS
        //}}
    }
	//{{DECLARE_CONTROLS
    //}}
}
