
import java.io.*;
import javax.swing.JTextArea;

public class JAcciseNetConsoleOutputStream extends OutputStream
{

    private JTextArea jta;
    private boolean attivo;

    public JAcciseNetConsoleOutputStream(JTextArea jta)
    {
        this.jta = jta;
        attivo = true;
    }

    public void close() throws IOException
    {
        attivo = false;
    }

    public void flush()
    {
    }

    public void write(byte[] b) throws IOException
    {
        write(b, 0, b.length);
    }

    public void write(byte[] b, int off, int len)
            throws IOException
    {
        if (attivo)
        {
            String s = "";
            for (int i = 0; i < len; i++)
            {
                s += (char) b[i + off];
            }
            if (jta != null)
            {
                jta.append(s);
                jta.setCaretPosition(jta.getDocument().getLength());
            }
            else
            {
                throw new IOException(
                        "No JTextArea specified for output");
            }
        }
    }

    public void write(int b) throws IOException
    {
        if (attivo)
        {
            if (jta != null)
            {
                if (jta.getText().length() > 32768)
                    jta.setText("");
                jta.append("" + ((char) b));
                jta.setCaretPosition(jta.getDocument().getLength());
            }
            else
            {
                throw new IOException(
                        "No JTextArea specified for output");
            }
        }
    }
}
