/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Giovanni
 */
import java.util.Hashtable;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

public class LDAPConnection {
    
     public LDAPConnection() {
    }

    
    
//    public static int CheckLDAPConnection(String user_name, String user_password, String url, String port, String domain) {
//        try {
//            Hashtable<String, String> env1 = new Hashtable<String, String>();
////            env1.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
////            env1.put(Context.SECURITY_AUTHENTICATION, "simple");
////            env1.put(Context.SECURITY_PRINCIPAL, user_name);
////            env1.put(Context.SECURITY_CREDENTIALS, user_password);
////            env1.put(Context.PROVIDER_URL, "ldap://172.16.94.97:389");
//
//            env1.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
//            env1.put(Context.PROVIDER_URL, "ldap://" + url + ":" + port);
////            env1.put(Context.PROVIDER_URL, "ldap://172.16.94.97:389");
//            env1.put(Context.SECURITY_AUTHENTICATION, "simple");
//            env1.put(Context.SECURITY_PRINCIPAL, user_name + "@" + domain);
////            env1.put(Context.SECURITY_PRINCIPAL, user_name + "@16.94");
//            env1.put(Context.SECURITY_CREDENTIALS, user_password);
//            env1.put("java.naming.ldap.version", "3");
//
//            try {
//                //Connect with ldap
//                InitialDirContext l = new InitialDirContext(env1);
//                //Connection succeeded
//                System.out.println("Connection succeeded!");
//                return 0;
//            } catch (AuthenticationException e) {
//                //Connection failed
//                System.out.println("Authentication failed!");
//                e.printStackTrace();
//                return 1;
//            } catch (NamingException namEx) {
//                System.out.println("LDAP connection failed!");
//                namEx.printStackTrace();
//                return 2;
//            }
//        } catch (Exception e) {
//            return 3;
//        }
//
//    }
//    
    public static int CheckLDAPSConnection(String user_name, String user_password, String url, String port, String domain) {
        try {
            Hashtable<String, String> env1 = new Hashtable<String, String>();
//            env1.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
//            env1.put(Context.SECURITY_AUTHENTICATION, "simple");
//            env1.put(Context.SECURITY_PRINCIPAL, user_name);
//            env1.put(Context.SECURITY_CREDENTIALS, user_password);
//            env1.put(Context.PROVIDER_URL, "ldap://172.16.94.97:389");

            env1.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            env1.put(Context.PROVIDER_URL, "ldaps://" + url + ":" + port);
            System.out.println("PROVURL: " + "ldaps://" + url + ":" + port);
//            env1.put(Context.PROVIDER_URL, "ldap://172.16.94.97:389");
            env1.put(Context.SECURITY_AUTHENTICATION, "simple");
            env1.put(Context.SECURITY_PRINCIPAL, user_name + "@" + domain);
            System.out.println("SECURITY_PRINCIPAL: " + user_name + "@" + domain);
//            env1.put(Context.SECURITY_PRINCIPAL, user_name + "@16.94");
            env1.put(Context.SECURITY_CREDENTIALS, user_password);
            System.out.println("SECURITY_PRINCIPAL: " + user_name + "@" + domain);
            env1.put("java.naming.ldap.version", "3");

            try {
                //Connect with ldap
                InitialDirContext l = new InitialDirContext(env1);
                //Connection succeeded
                System.out.println("Connection succeeded!");
                return 0;
            } catch (AuthenticationException e) {
                //Connection failed
                System.out.println("Authentication failed!");
                e.printStackTrace();
                return 1;
            } catch (NamingException namEx) {
                System.out.println("LDAP connection failed!");
                namEx.printStackTrace();
                return 2;
            }
        } catch (Exception e) {
            return 3;
        }

    }

    public static Record leggiNuovoUtente(String user_name, String user_password, String url, String port, String domain) {

        // Here we store the returned LDAP object data
        String dn = "";
        Record rec = new Record();
        // This will hold the returned attribute list
        Attributes attrs;
        Hashtable<String, String> env1 = new Hashtable<String, String>();
        env1.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env1.put(Context.PROVIDER_URL, "ldaps://" + url + ":" + port);
//        env1.put(Context.PROVIDER_URL, "ldap://172.16.94.97:389");
        env1.put(Context.SECURITY_AUTHENTICATION, "simple");
        env1.put(Context.SECURITY_PRINCIPAL, user_name + "@" + domain);
//        env1.put(Context.SECURITY_PRINCIPAL, user_name + "@intra.asl9");
        env1.put(Context.SECURITY_CREDENTIALS, user_password);
        env1.put("java.naming.ldap.version", "3");

        try {
            System.out.println("Connecting to host ");
            System.out.println();

            DirContext ctx = new InitialDirContext(env1);
            System.out.println("LDAP authentication successful!");

            // Specify the attribute list to be returned
            String[] attrIDs = {"name", "sn", "givenname"};

            SearchControls ctls = new SearchControls();
            ctls.setReturningAttributes(attrIDs);
            ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // Specify the search filter to match
            String filter = "(&(objectCategory=person)(objectClass=user)(name=" + user_name + "*))";
//      String filter = "(&(objectClass=m.angeletti)(sAMAccountName=intra))";

            // Search the subtree for objects using the given filter
            NamingEnumeration answer = ctx.search("DC=intra,DC=asl9", filter, ctls);

      // Print the answer
            //Search.printSearchEnumeration(answer);
            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();
                dn = sr.getName();
                attrs = sr.getAttributes();

                System.out.println("Found Object: " + dn + "," + "DC=intra,DC=asl9");
                if (attrs != null) {
                    // we have some attributes for this object
                    NamingEnumeration ae = attrs.getAll();

                    Attribute attr = (Attribute) ae.next();
                    String attrId = attr.getID();
                    Attribute utente = (Attribute) attrs.get("name");
                    Attribute cognome = (Attribute) attrs.get("sn");
                    Attribute nome = (Attribute) attrs.get("givenname");
                    System.out.println("Found Attribute: " + attrId);

                    String ut = utente.get().toString();
                    String cogn = "";
                    String nom = "";
                    if (cognome == null) {
                        cogn = ut;
                    } else {
                        cogn = cognome.get().toString();
                    }

                    if (nome == null) {
                        nom = ut;
                    } else {
                        nom = nome.get().toString();
                    }

                    rec.insElem("uutedominio", ut);
                    rec.insElem("unome", nom);
                    rec.insElem("ucognome", cogn);

                }
            }

            // Close the context when we're done
            ctx.close();
        } catch (AuthenticationException authEx) {
            authEx.printStackTrace();
            System.out.println("LDAP authentication failed!");
        } catch (NamingException namEx) {
            System.out.println("LDAP connection failed!");
            namEx.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rec;
    }

    public static void main(String[] arg) {
//        CheckLDAPConnection("p.contri", "Usl9grosseto");
//        leggiNuovoUtente("p.contri", "Usl9grosseto");
//        CheckLDAPConnection("m.angeletti", "Mauma111");
//        leggiNuovoUtente("m.angeletti", "Mauma111");
//        CheckLDAPSConnection("m.angeletti", "Mauma111","172.16.94.97","389","intra.asl9");
//        leggiNuovoUtente("m.angeletti", "Mauma111","172.16.94.97","389","intra.asl9");
//        CheckLDAPConnection("prova.michele", "Usl9grosseto");
//        leggiNuovoUtente("prova.michele", "Usl9grosseto");
    }
}
