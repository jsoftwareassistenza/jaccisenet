import java.lang.*;
import java.io.*;
import java.util.*;
import java.sql.*;

/**
 * Questa classe � utilizzata per incorporare in una struttura dati transitoria
 * i record fisici della base dati, al fine di costruire un'immagine del record
 * per consultarlo o per passare al db record da memorizzare
 * 
 * @author Paolo Ginanneschi
 * @version 1.0
 */

public class RecordJavaDb implements Serializable
{

    public static final int STRING = 1;
    public static final int SHORT = 2;
    public static final int INT = 3;
    public static final int LONG = 4;
    public static final int FLOAT = 5;
    public static final int DOUBLE = 6;
    public static final int BOOLEAN = 7;
    public static final int OBJECT = 8;
    public static final int VERO = 10;
    public static final int FALSO = 11;

    /**
     * Vettore lista campi
     */
    protected Vector contenuto;

    /**
     * indica se prima registrazione tabella
     * (vedi classe TabellaClient)
     */
    protected boolean primo;

    /**
     * indica se ultima registrazione tabella
     * (vedi classe TabellaClient)
     */
    protected boolean ultimo;

    /**
     * indica se record valido
     */
    protected boolean valido;

    /**
     * Costruttore: crea un record vuoto
     */

    public RecordJavaDb()
    {
        contenuto = new Vector();
        valido = true;
    
		//{{INIT_CONTROLS
		//}}
	}

    /**
     * Costruttore: crea un record a partite da un ResultSet
     * letto dal DB (vedi package java.sql).
     * L'oggetto viene riempito con i campi prelevati dalla posizione
     * corrente del ResultSet
     * 
     * @param rs ResultSet creato da un'interrogazione al db
     */

    public RecordJavaDb(ResultSet rs)
    {
        int tipo;
        
        try
        {
            contenuto = new Vector();
            valido = true;
            ResultSetMetaData rsmd = rs.getMetaData();
            for (int i = 1; i <= rsmd.getColumnCount();i++)
            {
                String nome = rsmd.getColumnName(i);
                nome = nome.toLowerCase();
                switch (rsmd.getColumnType(i))
                {
                    case Types.CHAR:
                        this.insElem(nome, rs.getString(i));
                        break;
                    case Types.VARCHAR:
                        this.insElem(nome, rs.getString(i));
                        break;
                    case Types.LONGVARCHAR:
                        this.insElem(nome, rs.getString(i));
                        break;
                    case Types.SMALLINT:
                        this.insElem(nome, rs.getShort(i));
                        break;
                    case Types.INTEGER:
                        this.insElem(nome, rs.getInt(i));
                        break;
                    case Types.BIGINT:
                        this.insElem(nome, rs.getLong(i));
                        break;
                    case Types.FLOAT:
                        this.insElem(nome, rs.getFloat(i)); 
                        break;
                    case Types.DOUBLE:
                        this.insElem(nome, rs.getDouble(i));
                        break;
                    case Types.OTHER:
                        this.insElem(nome, rs.getObject(i));
                        break;
                    case Types.DECIMAL:
                        this.insElem(nome, rs.getDouble(i));
                        break;
                        
                }
            }
        }
        catch (SQLException e)
        {
            System.out.println("Errore creazione record da db:" + e.getMessage());
        }
    }

    /**
     * Indica se il record prelevato � la prima registrazione
     * della tabella (vedi classe TabellaClient)
     * 
     * @return Vero o Falso
     */

    public boolean primo()
    {
        return primo;
    }        

    /**
     * Indica se il record prelevato  la prima registrazione
     * della tabella (vedi classe TabellaClient)
     * 
     * @return Vero o Falso
     */
    
    public boolean ultimo()
    {
        return ultimo;
    }        

    /**
     * Segna il record come prima registrazione
     * 
     * @param p True o False
     */

    public void marcaPrimo(boolean p)
    {
        primo = p;
    }        

    /**
     * Segna il record come ultima registrazione
     * 
     * @param u
     */

    public void marcaUltimo(boolean u)
    {
        ultimo = u;
    }        

    /**
     * marca il record come valido o no
     * 
     * @param v True o False
     */

    public void convalida(boolean v)
    {
        valido = v;
    }

    /**
     * Indica se è un record valido
     * 
     * @return True o False
     */

    public boolean valido()
    {
        return this.valido;
    }

    /**
     * Restituisce il numero dei campi nell'oggetto
     * 
     * @return numero campi
     */

    public long numeroCampi()
    {
        return contenuto.size();
    }

    /**
     * Restituisce oggetto campo all'indice i
     * 
     * @param i indice lista campi
     * @return Oggetto campo
     */

    public Campo leggiCampo(int i)
    {
        return ((Campo) contenuto.elementAt(i - 1));
    }

    /**
     * Restituisce il nome del campo
     * 
     * @param i indice lista campi
     * @return stringa nome
     */

    public String leggiNome(int i)
    {
        return ((Campo) contenuto.elementAt(i - 1)).nome();
    }

    /**
     * Restituisce oggetto valore di indice i
     * 
     * @param i indice lista campi
     * @return Oggetto valore
     */

    // restituisce valore del campo per indice
    public Object leggiValore(int i)
    {
        return ((Campo) contenuto.elementAt(i-1)).valore();
    }

    public int leggiTipo(int i)
    {
        return ((Campo) contenuto.elementAt(i-1)).tipo();
    }

    public int leggiTipo(String n)
    {
        n = n.toLowerCase();
        int i, j = 0;
        for (i = 0; i < contenuto.size();i++)
            if (((Campo) contenuto.elementAt(i)).nome().toLowerCase().equals(n))
            {
                j=i;
                i=999;
            }
        return ((Campo) contenuto.elementAt(j)).tipo();
    }

    /**
     * Restituisce oggetto valore da campo con nome
     * 
     * @param n stringa nome campo
     * @return Oggetto valore
     */

    public Object leggiValore(String n)
    {
        n = n.toLowerCase();
        int i, j = 0;
        for (i = 0; i < contenuto.size();i++)
            if (((Campo) contenuto.elementAt(i)).nome().toLowerCase().equals(n))
            {
                j=i;
                i=999;
            }
        return ((Campo) contenuto.elementAt(j)).valore();
    }

    /**
     * Inserisce oggetto nel record
     * 
     * @param nome Nome campo
     * @param cont Oggetto contenuto
     */

    public int insElem(String nome,Object cont)
    {
        String tipo = "";
        nome = nome.toLowerCase();
        tipo = cont.getClass().getName();
        if (tipo.equals("java.lang.String"))
            contenuto.addElement(new Campo(nome,cont,STRING));
        else if (tipo.equals("java.lang.Short"))
            contenuto.addElement(new Campo(nome,cont,SHORT));
        else if (tipo.equals("java.lang.Integer"))
            contenuto.addElement(new Campo(nome,cont,INT));
        else if (tipo.equals("java.lang.Long"))
            contenuto.addElement(new Campo(nome,cont,LONG));
        else if (tipo.equals("java.lang.Float"))
            contenuto.addElement(new Campo(nome,cont,FLOAT));
        else if (tipo.equals("java.lang.Double"))
            contenuto.addElement(new Campo(nome,cont,DOUBLE));
        return contenuto.size();
    }

    /**
     * Inserisce stringa nel record
     * 
     * @param nome Nome campo
     * @param cont stringa valore campo
     */

    public int insElem(String nome,String cont)
    {
        String ris = "";
        nome = nome.toLowerCase();
        int i = 0;
        if (nome != null && nome.length() > 0)
            for (i = 0; i < nome.length(); i++)
            {
                // caratteri speciali
                if (nome.substring(i, i + 1).equals("'"))
                    ris += "\'";
                else if (nome.substring(i, i + 1).equals("\""))
                    ris += "\"";
                else                    
                    ris += nome.substring(i, i + 1);
            }                
        contenuto.addElement(new Campo(nome, cont, STRING));
        return contenuto.size();
    }

    /**
     * Inserisce intero corto nel campo
     * 
     * @param nome Nome campo
     * @param cont Intero corto
     */

    public int insElem(String nome,short cont)
    {
        nome = nome.toLowerCase();
        contenuto.addElement(new Campo(nome,new Short(cont),SHORT));
        return contenuto.size();
    }

    /**
     * Inserisce intero nel campo
     * 
     * @param nome Nome campo
     * @param cont Intero
     */

    public int insElem(String nome,int cont)
    {
        nome = nome.toLowerCase();
        contenuto.addElement(new Campo(nome,new Integer(cont),INT));
        return contenuto.size();
    }

    /**
     * Inserisce intero lungo nel campo
     * 
     * @param nome Nome campo
     * @param cont Intero lungo
     */

    public int insElem(String nome,long cont)
    {
        nome = nome.toLowerCase();
        contenuto.addElement(new Campo(nome,new Long(cont),LONG));
        return contenuto.size();
    }

    /**
     * Inserisce virgola mobile prec.singola
     * 
     * @param nome Nome campo
     * @param cont Virgola mobile prec.singola
     */

    public int insElem(String nome,float cont)
    {
        nome = nome.toLowerCase();
        contenuto.addElement(new Campo(nome,new Float(cont),FLOAT));
        return contenuto.size();
    }

    /**
     * Inserisce virgola mobile doppia precisione
     * 
     * @param nome Nome campo
     * @param cont Virgola mobile prec.doppia
     */

    public int insElem(String nome,double cont)
    {
        nome = nome.toLowerCase();
        contenuto.addElement(new Campo(nome,new Double(cont),DOUBLE));
        return contenuto.size();
    }

    /**
     * Inserisce booleano
     * 
     * @param nome Nome campo
     * @param cont Booleano
     */
/*
    public int insElem(String nome,boolean cont)
    {
        contenuto.addElement(new Campo(nome,new Boolean(cont),BOOLEAN));
        return contenuto.size();
    }
*/

    /**
     * Restituisce campo stringa a indice i
     * 
     * @param i indice lista campi
     * @return Stringa
     */
    
    public String leggiStringa(int i)
    {
         if (i > contenuto.size())
         {
            System.out.println("Prelievo stringa: indice > max");
            return "";
         }
         else
            return (String) ((Campo) contenuto.elementAt(i - 1)).valore();
    }

    /**
     * Restituisce campo stringa associato a nome campo
     * 
     * @param nome Nome campo
     * @return stringa
     */

    public String leggiStringa(String nome)
    {
        nome = nome.toLowerCase();
        int i, j = 0;
        for (i = 0; i < contenuto.size();i++)
        {
            if (((Campo) contenuto.elementAt(i)).nome().toLowerCase().equals(nome))
            {
                j = i;
                i = Integer.MAX_VALUE;
                break;
            }
        }
        if (i == Integer.MAX_VALUE)
        {
            String str = (String) ((Campo) contenuto.elementAt(j)).valore();
            return str.trim();
        }
        else
        {
            System.out.println("Prelievo stringa: nome campo " + nome + " non trovato");
            return "";
        }
    }

    /**
     * Restituisce intero corto da indice campo
     * 
     * @param i indice lista campi
     * @return intero corto
     */

    public short leggiShort(int i)
    {
         if (i > contenuto.size())
         {
            System.out.println("Prelievo short: indice > max");
            return 0;
         }
         else
            return ((Number) ((Campo) contenuto.elementAt(i - 1)).valore()).shortValue();
    }

    /**
     * Restituisce intero corto da nome campo
     * 
     * @param nome Nome campo
     * @return intero corto
     */

    public short leggiShort(String nome)
    {
        nome = nome.toLowerCase();
        int i, j = 0;
        for (i = 0; i < contenuto.size();i++)
            if (((Campo) contenuto.elementAt(i)).nome().toLowerCase().equals(nome))
            {
                j = i;
                i = Integer.MAX_VALUE;
                break;
            }
        if (i == Integer.MAX_VALUE)            
            return ((Number) ((Campo) contenuto.elementAt(j)).valore()).shortValue();
        else
        {
            System.out.println("Prelievo short: nome campo " + nome + " non trovato");
            return 0;
        }
    }

    public boolean nullo(String nome)
    {
        nome = nome.toLowerCase();
        int i, j = 0;
        for (i = 0; i < contenuto.size();i++)
            if (((Campo) contenuto.elementAt(i)).nome().toLowerCase().equals(nome))
            {
                j = i;
                i = Integer.MAX_VALUE;
                break;
            }
        return (((Campo) contenuto.elementAt(j)).valore() == null);
    }        
 
    public boolean camponullo(int ind)
    {
        return (((Campo) contenuto.elementAt(ind)).valore() == null);
    }        

    public boolean esisteCampo(String nome)
    {
        nome = nome.toLowerCase();
        int i = 0;
        boolean ok = false;
        
        for (i = 0; i < contenuto.size(); i++)
        {
            if (((Campo) contenuto.elementAt(i)).nome().toLowerCase().equals(nome))
                ok = true;
        }            
        return ok;
    }        

    /**
     * restituisce intero
     * 
     * @param i indice lista campi
     * @return intero
     */

    public int leggiIntero(int i)
    {
         if (i > contenuto.size())
         {
            System.out.println("Prelievo int: indice > max");
            return 0;
         }
         else
            return ((Number) ((Campo) contenuto.elementAt(i - 1)).valore()).intValue();
    }

    /**
     * restituisce intero
     * 
     * @param nome nome campo
     * @return intero
     */

    public int leggiIntero(String nome)
    {
        nome = nome.toLowerCase();
        int i, j = 0;
        for (i = 0; i < contenuto.size();i++)
        {
            if (((Campo) contenuto.elementAt(i)).nome().toLowerCase().equals(nome))
            {
                j = i;
                i = Integer.MAX_VALUE;
                break;
            }
        }
        if (i == Integer.MAX_VALUE)
            return ((Number) ((Campo) contenuto.elementAt(j)).valore()).intValue();
        else
        {
            System.out.println("Prelievo int: nome campo " + nome + " non trovato");
            return 0;
        }
    }

    /**
     * restituisce intero lungo
     * 
     * @param i indice lista campi
     * @return intero lungo
     */

    public long leggiLong(int i)
    {
         if (i > contenuto.size())
         {
            System.out.println("Prelievo long: indice > max");
            return 0;
         }
         else
            return ((Number) ((Campo) contenuto.elementAt(i - 1)).valore()).longValue();
    }

    /**
     * restituisce intero lungo
     * 
     * @param nome nome campo
     * @return intero lungo
     */

    public long leggiLong(String nome)
    {
        nome = nome.toLowerCase();
        int i, j = 0;
        for (i = 0; i < contenuto.size();i++)
            if (((Campo) contenuto.elementAt(i)).nome().toLowerCase().equals(nome))
            {
                j = i;
                i = Integer.MAX_VALUE;
                break;
            }
        if (i == Integer.MAX_VALUE)            
            return ((Number) ((Campo) contenuto.elementAt(j)).valore()).longValue();
        else
        {
            System.out.println("Prelievo long: nome campo " + nome + " non trovato");
            return 0;
        }
    }

    /**
     * restituisce virgola mobile prec.singola
     * 
     * @param i indice lista campi
     * @return virgola mobile p.singola
     */

    public float leggiFloat(int i)
    {
         if (i > contenuto.size())
         {
            System.out.println("Prelievo float: indice > max");
            return 0;
         }
         else
            return ((Number) ((Campo) contenuto.elementAt(i - 1)).valore()).floatValue();
    }

    /**
     * restituisce virgola mobile prec.doppia
     * 
     * @param nome nome campo
     * @return virgola mobile prec.doppia
     */

    public float leggiFloat(String nome)
    {
        nome = nome.toLowerCase();
        int i, j = 0;
        for (i = 0; i < contenuto.size();i++)
            if (((Campo) contenuto.elementAt(i)).nome().toLowerCase().equals(nome))
            {
                j = i;
                i = Integer.MAX_VALUE;
                break;
            }
        if (i == Integer.MAX_VALUE)
            return ((Number) ((Campo) contenuto.elementAt(j)).valore()).floatValue();
        else
        {
            System.out.println("Prelievo float: nome campo " + nome + " non trovato");
            return 0;
        }
    }

    /**
     * restituisce virgola mobile prec.doppia
     * 
     * @param i indice lista campi
     * @return virgola mobile prec.doppia
     */

    public double leggiDouble(int i)
    {
         if (i > contenuto.size())
         {
            System.out.println("Prelievo double: indice > max");
            return 0;
         }
         else
            return ((Number) ((Campo) contenuto.elementAt(i - 1)).valore()).doubleValue();
    }

    /**
     * restituisce virgola mobile prec.doppia
     * 
     * @param nome nome campo
     * @return virgola mobile prec.doppia
     */

    public double leggiDouble(String nome)
    {
        nome = nome.toLowerCase();
        int i, j = 0;
        for (i = 0; i < contenuto.size();i++)
        {
            if (((Campo) contenuto.elementAt(i)).nome().toLowerCase().equals(nome))
            {
                j = i;
                i = Integer.MAX_VALUE;
                break;
            }
        }
        if (i == Integer.MAX_VALUE)
            return ((Number) ((Campo) contenuto.elementAt(j)).valore()).doubleValue();
        else
        {
            System.out.println("Prelievo double: nome campo " + nome + " non trovato");
            return 0;
        }
    }

    /**
     * confronta con altro record campo per campo
     * 
     * @param rConfr Oggetto Record su cui effettuare il confronto
     * @return True se uguali, False se diversi
     */

    public boolean confronta(RecordJavaDb rConfr)
    {
        int i = 0;
        String tipo = "";
        boolean esito = true;
                
        if (this.valido() != rConfr.valido())
            esito = false;
        else if (this.numeroCampi() != rConfr.numeroCampi())
            esito = false;
        else
        {
            for (i = 1; i <= this.numeroCampi(); i++)
            {
                if (!this.leggiValore(i).equals(rConfr.leggiValore(this.leggiNome(i))))
                {
                    esito = false;
                    i = 999;
                    break;
                }                    
            }
        }      
        return esito;
    }        

    public boolean confronta(RecordJavaDb rConfr, String campoId)
    {
        int i = 0;
        String tipo = "";
        boolean esito = true;
                
        if (!this.leggiValore(campoId).equals(rConfr.leggiValore(campoId)))
            esito = false;
        return esito;
    }        

		//{{DECLARE_CONTROLS
	//}}

    public void eliminaCampo(int i)
    {
        contenuto.removeElementAt(i);
    }

    public void eliminaCampo(String nome)
    {
        nome = nome.toLowerCase();
        int i, j = 0;
        for (i = 0; i < contenuto.size();i++)
            if (((Campo) contenuto.elementAt(i)).nome().toLowerCase().equals(nome))
            {
                j = i;
                i = Integer.MAX_VALUE;
                break;
            }
        if (i == Integer.MAX_VALUE)
            contenuto.removeElementAt(j);
        else
        {
            System.out.println("eliminazione campo: nome campo " + nome + " non trovato");
        }
    }
}