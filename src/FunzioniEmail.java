/*
 * EnvJAcciseNet.java
 *
 * Created on 31 marzo 2009, 18.25
 */

import java.sql.*;
import java.io.*;
import java.awt.*;
import java.lang.*;
import java.util.*;
import org.apache.derby.drda.NetworkServerControl;
import java.net.InetAddress;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import java.security.*;

/**
 *
 * @author  ginannaschi
 */
public class FunzioniEmail {
    
    public static boolean riceviEmail(String server_pop3, int porta_pop3, String username, String password, String cartella_copia)
    {
        boolean ok = true;
        try
        {
            Properties props = System.getProperties();
            Session session = Session.getDefaultInstance(props, null);             
            Store store = session.getStore("pop3");
            store.connect(server_pop3, username, password);            
            Folder folder = store.getDefaultFolder();
            if (folder != null)
            {
                folder = folder.getFolder("INBOX");
                if (folder != null)
                {
                    folder.open(Folder.READ_WRITE);
                    Message[] elencoMessaggi = folder.getMessages();
                    
                    System.out.println("n.messaggi:" + elencoMessaggi.length);
                    System.out.println("n.messaggi non letti:" + folder.getUnreadMessageCount());
                    for (int i = 0; i < elencoMessaggi.length; i++)
                    {
                        Message messaggio = elencoMessaggi[i];
                        String oggetto = messaggio.getSubject();
                        System.out.println("   oggetto: " + oggetto);
                        java.util.Date dt = messaggio.getSentDate();
                        System.out.println("   data invio: " + dt.toString());
                        System.out.println("   timestamp: " + dt.getTime());
                        if (oggetto.startsWith("JACCISE"))
                        {
                            String nomefile = oggetto.substring(8);
                            System.out.println("   nomefile: " + nomefile + "...");
                            MimeMultipart mp = (MimeMultipart) messaggio.getContent();
                            if (mp.getCount() > 1)
                            {
                                MimeBodyPart mbp = (MimeBodyPart) mp.getBodyPart(1);
                                File fdir = new File(cartella_copia + File.separator + "F" + (i + 1));
                                if (!fdir.exists())
                                    fdir.mkdir();
                                else
                                {
                                    String [] lff = fdir.list();
                                    for (int j = 0; j < lff.length; j++)
                                    {
                                        File fff = new File(cartella_copia + File.separator + "F" + (i + 1) + File.separator + lff[j]);
                                        fff.delete();
                                    }
                                }
                                FileOutputStream fos = new FileOutputStream(cartella_copia + File.separator + "F" + (i + 1) + File.separator + nomefile);
                                InputStream in = mbp.getInputStream();
                                byte[] b = new byte[1];
                                int ris = in.read(b);
                                while (ris != -1)
                                {
                                    fos.write(b);
                                    ris = in.read(b);
                                }
                                in.close();
                                fos.close();
                                System.out.println("   file salvato");
                                messaggio.setFlag(Flags.Flag.DELETED, true);
                            }
                        }
                        else
                        {
                            messaggio.setFlag(Flags.Flag.DELETED, true);
                        }
                    }
                    folder.close(true);
                }
            }
        }
        catch (Exception eemail)
        {
            ok = false;
            eemail.printStackTrace();
        }
        return ok;
    }
    
    public static String[] inviaEmail(String server_smtp, int porta_smtp, String username, String password,
        String mittente, String destinatario, String oggetto, String testo, String[] allegati)
    {
        String[] ris = new String[2];
        try
        {
            Properties props = System.getProperties();
            props.put("mail.smtp.host", server_smtp);
            props.put("mail.smtp.port", "" + porta_smtp);
            props.put("mail.debug", "true");
            if (!username.equals(""))
                props.put("mail.smtp.auth","true");            
            Session sessione = Session.getDefaultInstance(props, null);
            sessione.setDebug(true);
            MimeMessage msg = new MimeMessage(sessione);
            msg.setFrom(new InternetAddress(mittente));
            InternetAddress[] inddest = {new InternetAddress(destinatario)};
            msg.setRecipients(Message.RecipientType.TO, inddest);
            msg.setSubject(oggetto);
            msg.setSentDate(new java.util.Date());
            Multipart mp = new MimeMultipart();
            MimeBodyPart mbptxt = new MimeBodyPart();
            mbptxt.setText(testo);
            mp.addBodyPart(mbptxt);
            for (int i = 0; i < allegati.length; i++)
            {
                String nf = allegati[i];
                File f = new File(nf);
                if (f.exists())
                {
                    MimeBodyPart matt = new MimeBodyPart();
                    FileDataSource fds = new FileDataSource(nf);
                    matt.setDataHandler(new DataHandler(fds));
                    matt.setFileName(fds.getName());
                    mp.addBodyPart(matt);
                }
            }
            msg.setContent(mp);
            Transport t = sessione.getTransport("smtp");
            if (!username.equals(""))
            {
                t.connect(server_smtp, username, password);
            }
            else
                t.connect();
            t.sendMessage(msg, msg.getAllRecipients());
            t.close();
            ris[0] = "S";
            ris[1] = "";
        }
        catch (Exception eemail)
        {
            ris[0] = "N";
            ris[1] = eemail.getMessage();
        }
        return ris;
    }
    
}
