/*
 * EnvJAcciseNet.java
 *
 * Created on 31 marzo 2009, 18.25
 */

import java.sql.*;
import java.io.*;
import java.net.Proxy;
import org.apache.derby.drda.NetworkServerControl;

/**
 *
 * @author  ginannaschi
 */
public class EnvJAcciseNet {
    
    /** Creates a new instance of EnvJAcciseNet */
    public EnvJAcciseNet() {
    }
    
    public static  NetworkServerControl server_javadb = null;
    public static Connection conn = null;
    public static String indserver = "";
    public static String portadb = "1527";
    public static String postazione = "";
    public static String utente = "";
    public static String username = "";
    public static  boolean usaproxy = false;
    public static  String indproxy = "";
    public static  String portaproxy = "";
    public static  String tipoproxy = "";
    public static  boolean autproxy = false;
    public static  String userproxy = "";
    public static  String pwdproxy = "";
    public static  boolean usaproxys = false;
    public static  String indproxys = "";
    public static  String portaproxys = "";
    public static  String tipoproxys = "";
    public static  boolean autproxys = false;
    public static  String userproxys = "";
    public static  String pwdproxys = "";
    public static Proxy proxy = null;
    public static Proxy proxys = null;
    public static String login ="S";
    public static String urlldap ="";
    public static String portldap ="";
    public static String domainldap ="";
    
    
    public static String leggiProprieta(String pprog, String pprop) throws Exception {
        String val = "";
        Statement st = EnvJAcciseNet.conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT pval FROM proprieta WHERE pprog = '" + pprog + "' AND pprop = '" + pprop + "'");
        if (rs.next())
            val = rs.getString("pval");
        st.close();
        return val;
    }
    
    public static void salvaProprieta(String pprog, String pprop, String pval) throws Exception {
        PreparedStatement dst = EnvJAcciseNet.conn.prepareStatement("DELETE FROM proprieta WHERE pprog=? AND pprop=?");
        dst.clearParameters();
        dst.setString(1, pprog);
        dst.setString(2, pprop);
        dst.execute();
        dst.close();
        PreparedStatement pst = EnvJAcciseNet.conn.prepareStatement("INSERT INTO proprieta (pprog,pprop,pval) VALUES (?,?,?)");
        pst.clearParameters();
        pst.setString(1, pprog);
        pst.setString(2, pprop);
        pst.setString(3, pval);
        pst.execute();
        pst.close();
    }
    
    public static boolean copiaFile(String fOrig, String fDest) {
        byte[] b = new byte[8192];
        FileInputStream fis = null;
        FileOutputStream fos = null;
        boolean ok = true;
        int nBlocchi, nScarto;
        boolean scarto;
        int i;
        
        // controllo esistenza file di origine
        File fo = new File(fOrig);
        if (!fo.exists())
            return false;
        nBlocchi = (int) fo.length() / 8192;
        scarto = ((fo.length() % 8192) != 0);
        nScarto = (int) (fo.length() % 8192);
        // copia effettiva
        try {
            fis = new FileInputStream(fOrig);
            fos = new FileOutputStream(fDest);
            for (i = 0; i < nBlocchi; i++) {
                fis.read(b);
                fos.write(b);
            }
            // copia blocco di scarto
            if (scarto) {
                byte[] sc = new byte[nScarto];
                fis.read(sc);
                fos.write(sc);
            }
            fis.close();
            fos.close();
        }
        catch (Exception e) {
            ok = false;
            e.printStackTrace();
        }
        finally {
            try {
                fis.close();
                fos.close();
            }
            catch (IOException e) {
                ok = false;
            }
            return ok;
        }
    }
}
